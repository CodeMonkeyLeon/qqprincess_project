package com.az.love.aq.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.View
import com.az.love.aq.R
import com.az.love.aq.entity.Coordinate
import javax.sql.RowSetWriter


/**
 * @author lizheng.zhao
 * @date 2023/02/26
 *
 * @我知道
 * @你在一个地方
 * @在呼吸
 * @在笑
 * @在拍碎波浪送来的
 * @一千朵太阳
 */
class MyLittlePrincessView : View {

    /**
     * 画面尺寸 64x96
     */
    companion object {
        const val TAG = "AZ_LO_AQ"
        const val ROW_SIZE = 64
        const val COLUMN_SIZE = 96
        const val MAX_COUNT = 8
    }

    /**
     * 像素点数组
     */
    private val mCoordinateArray = Array(ROW_SIZE) { Array(COLUMN_SIZE) { Coordinate() } }

    /**
     * 画面宽
     */
    private var mWidth: Double = 0.0

    /**
     * 每个像素点的边长
     */
    private var mPixelsLength: Double = 0.0


    private var mCount: Int = 0

    /**
     * 是否是第一次进入
     */
    private var isFirstCome = true


    private lateinit var mContext: Context

    constructor(context: Context?) : super(context) {
        context?.let {
            mContext = it
        }
    }

    constructor(context: Context?, attr: AttributeSet?) : super(context, attr) {
        context?.let {
            mContext = it
        }
    }

    constructor(context: Context?, attr: AttributeSet?, def: Int) : super(context, attr, def) {
        context?.let {
            mContext = it
        }
    }


    /**
     * 测量
     */
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        if (isFirstCome) {
            isFirstCome = false
            val height = MeasureSpec.getSize(heightMeasureSpec)
            val width = MeasureSpec.getSize(widthMeasureSpec)
            mWidth = width.toDouble()
            mPixelsLength = mWidth / ROW_SIZE

            showLog("画面宽度: $mWidth        像素点边长: $mPixelsLength")
            //原点坐标
            val originX = 0
            val originY = 0
            //像素点数组坐标
            for (i in 0 until ROW_SIZE) {
                for (j in 0 until COLUMN_SIZE) {
                    mCoordinateArray[i][j].setSideLength(mPixelsLength.toFloat())
                    mCoordinateArray[i][j].setX((originX + i * mPixelsLength).toFloat())
                    mCoordinateArray[i][j].setY((originY + j * mPixelsLength).toFloat())
                }
            }
            setColor()
        }
    }


    fun setCount(count: Int) {
        if (count <= MAX_COUNT) {
            mCount = count
            invalidate()
        }
    }


    /**
     * 绘制
     */
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.let { c ->
            if (mCount <= MAX_COUNT) {

                for (i in 0 until ROW_SIZE) {
                    for (j in 0 until mCount * 12) {
                        drawPixels(mCoordinateArray[i][j], c)
                    }
                }


//                for(j in 0 until COLUMN_SIZE){
//                    for (i in 0 until  mCount*8){
//                        drawPixels(mCoordinateArray[i][j], c)
//                    }
//                }
            }
        }
    }


    /**
     * 绘制单个像素点
     */
    private fun drawPixels(coordinate: Coordinate, canvas: Canvas) {
        val paint = Paint()
        paint.color = mContext.getColor(coordinate.getColor())
        paint.style = Paint.Style.FILL
        canvas.drawRect(
            coordinate.getLeftTopX(),
            coordinate.getLeftTopY(),
            coordinate.getRightBottomX(),
            coordinate.getRightBottomY(),
            paint
        )
    }


    private fun setColor() {
        //绘制第1列
        draw1Column()
        //绘制第2列
        draw2Column()
        //绘制第3列
        draw3Column()
        //绘制第4列
        draw4Column()
        //绘制第5列
        draw5Column()
        //绘制第6列
        draw6Column()
        //绘制第7列
        draw7Column()
        //绘制第8列
        draw8Column()
        //绘制第9列
        draw9Column()
        //绘制第10列
        draw10Column()
        //绘制第11列
        draw11Column()
        //绘制第12列
        draw12Column()
        //绘制第13列
        draw13Column()
        //绘制第14列
        draw14Column()
        //绘制第15列
        draw15Column()
        //绘制第16列
        draw16Column()
        //绘制第17列
        draw17Column()
        //绘制第18列
        draw18Column()
        //绘制第19列
        draw19Column()
        //绘制第20列
        draw20Column()
        //绘制第21列
        draw21Column()
        //绘制第22列
        draw22Column()
        //绘制第23列
        draw23Column()
        //绘制第24列
        draw24Column()
        //绘制第25列
        draw25Column()
        //绘制第26列
        draw26Column()
        //绘制第27列
        draw27Column()
        //绘制第28列
        draw28Column()
        //绘制第29列
        draw29Column()
        //绘制第30列
        draw30Column()
        //绘制第31列
        draw31Column()
        //绘制第32列
        draw32Column()
        //绘制第33列
        draw33Column()
        //绘制第34列
        draw34Column()
        //绘制第35列
        draw35Column()
        //绘制第36列
        draw36Column()
        //绘制第37列
        draw37Column()
        //绘制第38列
        draw38Column()
        //绘制第39列
        draw39Column()
        //绘制第40列
        draw40Column()
        //绘制第41列
        draw41Column()
        //绘制第42列
        draw42Column()
        //绘制第43列
        draw43Column()
        //绘制第44列
        draw44Column()
        //绘制第45列
        draw45Column()
        //绘制第46列
        draw46Column()
        //绘制第47列
        draw47Column()
        //绘制第48列
        draw48Column()
        //绘制第49列
        draw49Column()
        //绘制第50列
        draw50Column()
        //绘制第51列
        draw51Column()
        //绘制第52列
        draw52Column()
        //绘制第53列
        draw53Column()
        //绘制第54列
        draw54Column()
        //绘制第55列
        draw55Column()
        //绘制第56列
        draw56Column()
        //绘制第57列
        draw57Column()
        //绘制第58列
        draw58Column()
        //绘制第59列
        draw59Column()
        //绘制第60列
        draw60Column()
        //绘制第61列
        draw61Column()
        //绘制第62列
        draw62Column()
        //绘制第63列
        draw63Column()
        //绘制第64列
        draw64Column()
    }


    /**
     * 绘制第64列
     */
    private fun draw64Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[63][j]
            if (j < 3) {
                setPixelsColor(R.color.color_princess_3, 63, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_6, 63, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_5, 63, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_3, 63, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_5, 63, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_10, 63, j)
            } else if (j < 27) {
                setPixelsColor(R.color.color_princess_9, 63, j)
            } else if (j < 28) {
                setPixelsColor(R.color.color_princess_10, 63, j)
            } else if (j < 29) {
                setPixelsColor(R.color.color_princess_5, 63, j)
            } else if (j < 32) {
                setPixelsColor(R.color.color_princess_3, 63, j)
            } else if (j < 79) {
                setPixelsColor(R.color.color_princess_6, 63, j)
            } else if (j < 80) {
                setPixelsColor(R.color.color_princess_7, 63, j)
            } else if (j < 81) {
                setPixelsColor(R.color.color_princess_6, 63, j)
            } else if (j < 82) {
                setPixelsColor(R.color.color_princess_7, 63, j)
            } else if (j < 83) {
                setPixelsColor(R.color.color_princess_9, 63, j)
            } else if (j < 85) {
                setPixelsColor(R.color.color_princess_6, 63, j)
            } else if (j < 86) {
                setPixelsColor(R.color.color_princess_9, 63, j)
            } else if (j < 87) {
                setPixelsColor(R.color.color_princess_7, 63, j)
            } else if (j < 89) {
                setPixelsColor(R.color.color_princess_6, 63, j)
            } else if (j < 91) {
                setPixelsColor(R.color.color_princess_7, 63, j)
            } else if (j < 93) {
                setPixelsColor(R.color.color_princess_6, 63, j)
            } else if (j < 94) {
                setPixelsColor(R.color.color_princess_7, 63, j)
            } else if (j < 95) {
                setPixelsColor(R.color.color_princess_9, 63, j)
            } else {
                setPixelsColor(R.color.color_princess_7, 63, j)
            }
        }
    }


    /**
     * 绘制第63列
     */
    private fun draw63Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[62][j]
            if (j < 2) {
                setPixelsColor(R.color.color_princess_3, 62, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_6, 62, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_5, 62, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_6, 62, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_10, 62, j)
            } else if (j < 26) {
                setPixelsColor(R.color.color_princess_9, 62, j)
            } else if (j < 27) {
                setPixelsColor(R.color.color_princess_10, 62, j)
            } else if (j < 29) {
                setPixelsColor(R.color.color_princess_5, 62, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_3, 62, j)
            } else if (j < 78) {
                setPixelsColor(R.color.color_princess_6, 62, j)
            } else if (j < 79) {
                setPixelsColor(R.color.color_princess_7, 62, j)
            } else if (j < 80) {
                setPixelsColor(R.color.color_princess_6, 62, j)
            } else if (j < 81) {
                setPixelsColor(R.color.color_princess_7, 62, j)
            } else if (j < 82) {
                setPixelsColor(R.color.color_princess_9, 62, j)
            } else if (j < 83) {
                setPixelsColor(R.color.color_princess_7, 62, j)
            } else if (j < 84) {
                setPixelsColor(R.color.color_princess_6, 62, j)
            } else if (j < 86) {
                setPixelsColor(R.color.color_princess_9, 62, j)
            } else if (j < 88) {
                setPixelsColor(R.color.color_princess_6, 62, j)
            } else if (j < 91) {
                setPixelsColor(R.color.color_princess_7, 62, j)
            } else if (j < 92) {
                setPixelsColor(R.color.color_princess_6, 62, j)
            } else if (j < 94) {
                setPixelsColor(R.color.color_princess_7, 62, j)
            } else if (j < 95) {
                setPixelsColor(R.color.color_princess_9, 62, j)
            } else {
                setPixelsColor(R.color.color_princess_7, 62, j)
            }
        }
    }


    /**
     * 绘制第62列
     */
    private fun draw62Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[61][j]
            if (j < 2) {
                setPixelsColor(R.color.color_princess_3, 61, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_6, 61, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_5, 61, j)
            } else if (j < 24) {
                setPixelsColor(R.color.color_princess_10, 61, j)
            } else if (j < 26) {
                setPixelsColor(R.color.color_princess_9, 61, j)
            } else if (j < 27) {
                setPixelsColor(R.color.color_princess_6, 61, j)
            } else if (j < 28) {
                setPixelsColor(R.color.color_princess_9, 61, j)
            } else if (j < 29) {
                setPixelsColor(R.color.color_princess_6, 61, j)
            } else if (j < 30) {
                setPixelsColor(R.color.color_princess_5, 61, j)
            } else if (j < 31) {
                setPixelsColor(R.color.color_princess_9, 61, j)
            } else if (j < 32) {
                setPixelsColor(R.color.color_princess_2, 61, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_6, 61, j)
            } else if (j < 35) {
                setPixelsColor(R.color.color_princess_5, 61, j)
            } else if (j < 77) {
                setPixelsColor(R.color.color_princess_6, 61, j)
            } else if (j < 78) {
                setPixelsColor(R.color.color_princess_7, 61, j)
            } else if (j < 79) {
                setPixelsColor(R.color.color_princess_6, 61, j)
            } else if (j < 80) {
                setPixelsColor(R.color.color_princess_7, 61, j)
            } else if (j < 81) {
                setPixelsColor(R.color.color_princess_9, 61, j)
            } else if (j < 82) {
                setPixelsColor(R.color.color_princess_7, 61, j)
            } else if (j < 83) {
                setPixelsColor(R.color.color_princess_6, 61, j)
            } else if (j < 85) {
                setPixelsColor(R.color.color_princess_9, 61, j)
            } else if (j < 86) {
                setPixelsColor(R.color.color_princess_7, 61, j)
            } else if (j < 87) {
                setPixelsColor(R.color.color_princess_6, 61, j)
            } else if (j < 91) {
                setPixelsColor(R.color.color_princess_7, 61, j)
            } else if (j < 93) {
                setPixelsColor(R.color.color_princess_6, 61, j)
            } else if (j < 94) {
                setPixelsColor(R.color.color_princess_7, 61, j)
            } else {
                setPixelsColor(R.color.color_princess_9, 61, j)
            }
        }
    }


    /**
     * 绘制第61列
     */
    private fun draw61Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[60][j]
            if (j < 1) {
                setPixelsColor(R.color.color_princess_3, 60, j)
            } else if (j < 16) {
                setPixelsColor(R.color.color_princess_6, 60, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_3, 60, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_6, 60, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_3, 60, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_6, 60, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_10, 60, j)
            } else if (j < 26) {
                setPixelsColor(R.color.color_princess_9, 60, j)
            } else if (j < 27) {
                setPixelsColor(R.color.color_princess_5, 60, j)
            } else if (j < 28) {
                setPixelsColor(R.color.color_princess_6, 60, j)
            } else if (j < 29) {
                setPixelsColor(R.color.color_princess_9, 60, j)
            } else if (j < 30) {
                setPixelsColor(R.color.color_princess_10, 60, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_2, 60, j)
            } else if (j < 35) {
                setPixelsColor(R.color.color_princess_4, 60, j)
            } else if (j < 37) {
                setPixelsColor(R.color.color_princess_2, 60, j)
            } else if (j < 76) {
                setPixelsColor(R.color.color_princess_6, 60, j)
            } else if (j < 77) {
                setPixelsColor(R.color.color_princess_7, 60, j)
            } else if (j < 78) {
                setPixelsColor(R.color.color_princess_6, 60, j)
            } else if (j < 79) {
                setPixelsColor(R.color.color_princess_7, 60, j)
            } else if (j < 80) {
                setPixelsColor(R.color.color_princess_9, 60, j)
            } else if (j < 81) {
                setPixelsColor(R.color.color_princess_7, 60, j)
            } else if (j < 82) {
                setPixelsColor(R.color.color_princess_6, 60, j)
            } else if (j < 84) {
                setPixelsColor(R.color.color_princess_9, 60, j)
            } else if (j < 85) {
                setPixelsColor(R.color.color_princess_7, 60, j)
            } else if (j < 87) {
                setPixelsColor(R.color.color_princess_6, 60, j)
            } else if (j < 91) {
                setPixelsColor(R.color.color_princess_7, 60, j)
            } else if (j < 92) {
                setPixelsColor(R.color.color_princess_6, 60, j)
            } else if (j < 95) {
                setPixelsColor(R.color.color_princess_7, 60, j)
            } else {
                setPixelsColor(R.color.color_princess_9, 60, j)
            }
        }
    }


    /**
     * 绘制第60列
     */
    private fun draw60Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[59][j]
            if (j < 3) {
                setPixelsColor(R.color.color_princess_3, 59, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_6, 59, j)
            } else if (j < 13) {
                setPixelsColor(R.color.color_princess_5, 59, j)
            } else if (j < 14) {
                setPixelsColor(R.color.color_princess_6, 59, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_3, 59, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_5, 59, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_6, 59, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_10, 59, j)
            } else if (j < 25) {
                setPixelsColor(R.color.color_princess_9, 59, j)
            } else if (j < 26) {
                setPixelsColor(R.color.color_princess_10, 59, j)
            } else if (j < 27) {
                setPixelsColor(R.color.color_princess_3, 59, j)
            } else if (j < 28) {
                setPixelsColor(R.color.color_princess_5, 59, j)
            } else if (j < 29) {
                setPixelsColor(R.color.color_princess_6, 59, j)
            } else if (j < 30) {
                setPixelsColor(R.color.color_princess_9, 59, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_2, 59, j)
            } else if (j < 35) {
                setPixelsColor(R.color.color_princess_4, 59, j)
            } else if (j < 39) {
                setPixelsColor(R.color.color_princess_2, 59, j)
            } else if (j < 40) {
                setPixelsColor(R.color.color_princess_9, 59, j)
            } else if (j < 76) {
                setPixelsColor(R.color.color_princess_6, 59, j)
            } else if (j < 78) {
                setPixelsColor(R.color.color_princess_7, 59, j)
            } else if (j < 79) {
                setPixelsColor(R.color.color_princess_9, 59, j)
            } else if (j < 80) {
                setPixelsColor(R.color.color_princess_7, 59, j)
            } else if (j < 81) {
                setPixelsColor(R.color.color_princess_6, 59, j)
            } else if (j < 82) {
                setPixelsColor(R.color.color_princess_7, 59, j)
            } else if (j < 83) {
                setPixelsColor(R.color.color_princess_9, 59, j)
            } else if (j < 84) {
                setPixelsColor(R.color.color_princess_7, 59, j)
            } else if (j < 86) {
                setPixelsColor(R.color.color_princess_6, 59, j)
            } else if (j < 90) {
                setPixelsColor(R.color.color_princess_7, 59, j)
            } else if (j < 92) {
                setPixelsColor(R.color.color_princess_6, 59, j)
            } else if (j < 95) {
                setPixelsColor(R.color.color_princess_7, 59, j)
            } else {
                setPixelsColor(R.color.color_princess_9, 59, j)
            }
        }
    }


    /**
     * 绘制第59列
     */
    private fun draw59Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[58][j]
            if (j < 1) {
                setPixelsColor(R.color.color_princess_5, 58, j)
            } else if (j < 2) {
                setPixelsColor(R.color.color_princess_3, 58, j)
            } else if (j < 16) {
                setPixelsColor(R.color.color_princess_6, 58, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_3, 58, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_5, 58, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_6, 58, j)
            } else if (j < 26) {
                setPixelsColor(R.color.color_princess_9, 58, j)
            } else if (j < 27) {
                setPixelsColor(R.color.color_princess_5, 58, j)
            } else if (j < 28) {
                setPixelsColor(R.color.color_princess_3, 58, j)
            } else if (j < 29) {
                setPixelsColor(R.color.color_princess_9, 58, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_2, 58, j)
            } else if (j < 37) {
                setPixelsColor(R.color.color_princess_4, 58, j)
            } else if (j < 41) {
                setPixelsColor(R.color.color_princess_2, 58, j)
            } else if (j < 42) {
                setPixelsColor(R.color.color_princess_9, 58, j)
            } else if (j < 75) {
                setPixelsColor(R.color.color_princess_6, 58, j)
            } else if (j < 77) {
                setPixelsColor(R.color.color_princess_7, 58, j)
            } else if (j < 78) {
                setPixelsColor(R.color.color_princess_9, 58, j)
            } else if (j < 79) {
                setPixelsColor(R.color.color_princess_7, 58, j)
            } else if (j < 81) {
                setPixelsColor(R.color.color_princess_6, 58, j)
            } else if (j < 82) {
                setPixelsColor(R.color.color_princess_7, 58, j)
            } else if (j < 83) {
                setPixelsColor(R.color.color_princess_9, 58, j)
            } else if (j < 85) {
                setPixelsColor(R.color.color_princess_6, 58, j)
            } else if (j < 89) {
                setPixelsColor(R.color.color_princess_7, 58, j)
            } else if (j < 93) {
                setPixelsColor(R.color.color_princess_6, 58, j)
            } else if (j < 94) {
                setPixelsColor(R.color.color_princess_7, 58, j)
            } else {
                setPixelsColor(R.color.color_princess_9, 58, j)
            }
        }
    }


    /**
     * 绘制第58列
     */
    private fun draw58Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[57][j]
            if (j < 13) {
                setPixelsColor(R.color.color_princess_6, 57, j)
            } else if (j < 14) {
                setPixelsColor(R.color.color_princess_5, 57, j)
            } else if (j < 15) {
                setPixelsColor(R.color.color_princess_6, 57, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_5, 57, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_6, 57, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_10, 57, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_9, 57, j)
            } else if (j < 25) {
                setPixelsColor(R.color.color_princess_2, 57, j)
            } else if (j < 26) {
                setPixelsColor(R.color.color_princess_9, 57, j)
            } else if (j < 29) {
                setPixelsColor(R.color.color_princess_2, 57, j)
            } else if (j < 35) {
                setPixelsColor(R.color.color_princess_4, 57, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 57, j)
            } else if (j < 44) {
                setPixelsColor(R.color.color_princess_9, 57, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_7, 57, j)
            } else if (j < 74) {
                setPixelsColor(R.color.color_princess_6, 57, j)
            } else if (j < 85) {
                setPixelsColor(R.color.color_princess_1, 57, j)
            } else if (j < 90) {
                setPixelsColor(R.color.color_princess_7, 57, j)
            } else if (j < 93) {
                setPixelsColor(R.color.color_princess_6, 57, j)
            } else if (j < 95) {
                setPixelsColor(R.color.color_princess_7, 57, j)
            } else {
                setPixelsColor(R.color.color_princess_9, 57, j)
            }
        }
    }


    /**
     * 绘制第57列
     */
    private fun draw57Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[56][j]
            if (j < 1) {
                setPixelsColor(R.color.color_princess_5, 56, j)
            } else if (j < 13) {
                setPixelsColor(R.color.color_princess_6, 56, j)
            } else if (j < 15) {
                setPixelsColor(R.color.color_princess_5, 56, j)
            } else if (j < 16) {
                setPixelsColor(R.color.color_princess_6, 56, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_5, 56, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_6, 56, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_9, 56, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_2, 56, j)
            } else if (j < 35) {
                setPixelsColor(R.color.color_princess_4, 56, j)
            } else if (j < 37) {
                setPixelsColor(R.color.color_princess_2, 56, j)
            } else if (j < 38) {
                setPixelsColor(R.color.color_princess_9, 56, j)
            } else if (j < 40) {
                setPixelsColor(R.color.color_princess_2, 56, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_4, 56, j)
            } else if (j < 46) {
                setPixelsColor(R.color.color_princess_9, 56, j)
            } else if (j < 73) {
                setPixelsColor(R.color.color_princess_6, 56, j)
            } else if (j < 90) {
                setPixelsColor(R.color.color_princess_1, 56, j)
            } else if (j < 95) {
                setPixelsColor(R.color.color_princess_6, 56, j)
            } else {
                setPixelsColor(R.color.color_princess_7, 56, j)
            }
        }
    }


    /**
     * 绘制第56列
     */
    private fun draw56Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[55][j]
            if (j < 2) {
                setPixelsColor(R.color.color_princess_3, 55, j)
            } else if (j < 8) {
                setPixelsColor(R.color.color_princess_6, 55, j)
            } else if (j < 9) {
                setPixelsColor(R.color.color_princess_3, 55, j)
            } else if (j < 16) {
                setPixelsColor(R.color.color_princess_6, 55, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_5, 55, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_6, 55, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_10, 55, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_9, 55, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_2, 55, j)
            } else if (j < 31) {
                setPixelsColor(R.color.color_princess_4, 55, j)
            } else if (j < 33) {
                setPixelsColor(R.color.color_princess_2, 55, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_4, 55, j)
            } else if (j < 36) {
                setPixelsColor(R.color.color_princess_2, 55, j)
            } else if (j < 37) {
                setPixelsColor(R.color.color_princess_9, 55, j)
            } else if (j < 39) {
                setPixelsColor(R.color.color_princess_10, 55, j)
            } else if (j < 44) {
                setPixelsColor(R.color.color_princess_4, 55, j)
            } else if (j < 47) {
                setPixelsColor(R.color.color_princess_2, 55, j)
            } else if (j < 72) {
                setPixelsColor(R.color.color_princess_6, 55, j)
            } else if (j < 81) {
                setPixelsColor(R.color.color_princess_1, 55, j)
            } else if (j < 83) {
                setPixelsColor(R.color.color_princess_7, 55, j)
            } else if (j < 86) {
                setPixelsColor(R.color.color_princess_1, 55, j)
            } else if (j < 90) {
                setPixelsColor(R.color.color_princess_7, 55, j)
            } else if (j < 93) {
                setPixelsColor(R.color.color_princess_1, 55, j)
            } else if (j < 95) {
                setPixelsColor(R.color.color_princess_6, 55, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 55, j)
            }
        }
    }


    /**
     * 绘制第55列
     */
    private fun draw55Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[54][j]
            if (j < 1) {
                setPixelsColor(R.color.color_princess_5, 54, j)
            } else if (j < 2) {
                setPixelsColor(R.color.color_princess_6, 54, j)
            } else if (j < 3) {
                setPixelsColor(R.color.color_princess_3, 54, j)
            } else if (j < 13) {
                setPixelsColor(R.color.color_princess_6, 54, j)
            } else if (j < 14) {
                setPixelsColor(R.color.color_princess_5, 54, j)
            } else if (j < 15) {
                setPixelsColor(R.color.color_princess_6, 54, j)
            } else if (j < 16) {
                setPixelsColor(R.color.color_princess_5, 54, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_6, 54, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_10, 54, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_9, 54, j)
            } else if (j < 35) {
                setPixelsColor(R.color.color_princess_2, 54, j)
            } else if (j < 36) {
                setPixelsColor(R.color.color_princess_9, 54, j)
            } else if (j < 38) {
                setPixelsColor(R.color.color_princess_10, 54, j)
            } else if (j < 39) {
                setPixelsColor(R.color.color_princess_9, 54, j)
            } else if (j < 40) {
                setPixelsColor(R.color.color_princess_2, 54, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_4, 54, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_11, 54, j)
            } else if (j < 46) {
                setPixelsColor(R.color.color_princess_10, 54, j)
            } else if (j < 47) {
                setPixelsColor(R.color.color_princess_9, 54, j)
            } else if (j < 48) {
                setPixelsColor(R.color.color_princess_2, 54, j)
            } else if (j < 49) {
                setPixelsColor(R.color.color_princess_9, 54, j)
            } else if (j < 70) {
                setPixelsColor(R.color.color_princess_6, 54, j)
            } else if (j < 83) {
                setPixelsColor(R.color.color_princess_1, 54, j)
            } else if (j < 91) {
                setPixelsColor(R.color.color_princess_7, 54, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 54, j)
            }
        }
    }


    /**
     * 绘制第54列
     */
    private fun draw54Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[53][j]
            if (j < 1) {
                setPixelsColor(R.color.color_princess_6, 53, j)
            } else if (j < 2) {
                setPixelsColor(R.color.color_princess_3, 53, j)
            } else if (j < 13) {
                setPixelsColor(R.color.color_princess_6, 53, j)
            } else if (j < 14) {
                setPixelsColor(R.color.color_princess_5, 53, j)
            } else if (j < 15) {
                setPixelsColor(R.color.color_princess_3, 53, j)
            } else if (j < 16) {
                setPixelsColor(R.color.color_princess_6, 53, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_5, 53, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_10, 53, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_9, 53, j)
            } else if (j < 35) {
                setPixelsColor(R.color.color_princess_2, 53, j)
            } else if (j < 36) {
                setPixelsColor(R.color.color_princess_9, 53, j)
            } else if (j < 37) {
                setPixelsColor(R.color.color_princess_5, 53, j)
            } else if (j < 39) {
                setPixelsColor(R.color.color_princess_10, 53, j)
            } else if (j < 40) {
                setPixelsColor(R.color.color_princess_2, 53, j)
            } else if (j < 42) {
                setPixelsColor(R.color.color_princess_4, 53, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 53, j)
            } else if (j < 44) {
                setPixelsColor(R.color.color_princess_11, 53, j)
            } else if (j < 47) {
                setPixelsColor(R.color.color_princess_5, 53, j)
            } else if (j < 49) {
                setPixelsColor(R.color.color_princess_2, 53, j)
            } else if (j < 68) {
                setPixelsColor(R.color.color_princess_6, 53, j)
            } else if (j < 69) {
                setPixelsColor(R.color.color_princess_7, 53, j)
            } else if (j < 88) {
                setPixelsColor(R.color.color_princess_1, 53, j)
            } else {
                setPixelsColor(R.color.color_princess_7, 53, j)
            }
        }
    }


    /**
     * 绘制第53列
     */
    private fun draw53Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[52][j]
            if (j < 1) {
                setPixelsColor(R.color.color_princess_6, 52, j)
            } else if (j < 2) {
                setPixelsColor(R.color.color_princess_5, 52, j)
            } else if (j < 8) {
                setPixelsColor(R.color.color_princess_6, 52, j)
            } else if (j < 9) {
                setPixelsColor(R.color.color_princess_3, 52, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_6, 52, j)
            } else if (j < 15) {
                setPixelsColor(R.color.color_princess_5, 52, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_10, 52, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_9, 52, j)
            } else if (j < 30) {
                setPixelsColor(R.color.color_princess_2, 52, j)
            } else if (j < 33) {
                setPixelsColor(R.color.color_princess_9, 52, j)
            } else if (j < 35) {
                setPixelsColor(R.color.color_princess_2, 52, j)
            } else if (j < 36) {
                setPixelsColor(R.color.color_princess_9, 52, j)
            } else if (j < 37) {
                setPixelsColor(R.color.color_princess_5, 52, j)
            } else if (j < 38) {
                setPixelsColor(R.color.color_princess_10, 52, j)
            } else if (j < 39) {
                setPixelsColor(R.color.color_princess_9, 52, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 52, j)
            } else if (j < 44) {
                setPixelsColor(R.color.color_princess_11, 52, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_5, 52, j)
            } else if (j < 46) {
                setPixelsColor(R.color.color_princess_12, 52, j)
            } else if (j < 47) {
                setPixelsColor(R.color.color_princess_11, 52, j)
            } else if (j < 48) {
                setPixelsColor(R.color.color_princess_12, 52, j)
            } else if (j < 49) {
                setPixelsColor(R.color.color_princess_9, 52, j)
            } else if (j < 50) {
                setPixelsColor(R.color.color_princess_2, 52, j)
            } else if (j < 67) {
                setPixelsColor(R.color.color_princess_6, 52, j)
            } else if (j < 68) {
                setPixelsColor(R.color.color_princess_7, 52, j)
            } else if (j < 93) {
                setPixelsColor(R.color.color_princess_1, 52, j)
            } else {
                setPixelsColor(R.color.color_princess_7, 52, j)
            }
        }
    }


    /**
     * 绘制第52列
     */
    private fun draw52Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[51][j]
            if (j < 1) {
                setPixelsColor(R.color.color_princess_5, 51, j)
            } else if (j < 8) {
                setPixelsColor(R.color.color_princess_6, 51, j)
            } else if (j < 9) {
                setPixelsColor(R.color.color_princess_3, 51, j)
            } else if (j < 10) {
                setPixelsColor(R.color.color_princess_6, 51, j)
            } else if (j < 11) {
                setPixelsColor(R.color.color_princess_3, 51, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_5, 51, j)
            } else if (j < 13) {
                setPixelsColor(R.color.color_princess_3, 51, j)
            } else if (j < 14) {
                setPixelsColor(R.color.color_princess_6, 51, j)
            } else if (j < 15) {
                setPixelsColor(R.color.color_princess_5, 51, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_10, 51, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_9, 51, j)
            } else if (j < 30) {
                setPixelsColor(R.color.color_princess_2, 51, j)
            } else if (j < 36) {
                setPixelsColor(R.color.color_princess_9, 51, j)
            } else if (j < 37) {
                setPixelsColor(R.color.color_princess_5, 51, j)
            } else if (j < 38) {
                setPixelsColor(R.color.color_princess_10, 51, j)
            } else if (j < 39) {
                setPixelsColor(R.color.color_princess_9, 51, j)
            } else if (j < 42) {
                setPixelsColor(R.color.color_princess_2, 51, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_9, 51, j)
            } else if (j < 44) {
                setPixelsColor(R.color.color_princess_11, 51, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_5, 51, j)
            } else if (j < 48) {
                setPixelsColor(R.color.color_princess_12, 51, j)
            } else if (j < 49) {
                setPixelsColor(R.color.color_princess_11, 51, j)
            } else if (j < 50) {
                setPixelsColor(R.color.color_princess_9, 51, j)
            } else if (j < 51) {
                setPixelsColor(R.color.color_princess_2, 51, j)
            } else if (j < 52) {
                setPixelsColor(R.color.color_princess_9, 51, j)
            } else if (j < 67) {
                setPixelsColor(R.color.color_princess_6, 51, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 51, j)
            }
        }
    }


    /**
     * 绘制第51列
     */
    private fun draw51Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[50][j]
            if (j < 7) {
                setPixelsColor(R.color.color_princess_6, 50, j)
            } else if (j < 9) {
                setPixelsColor(R.color.color_princess_3, 50, j)
            } else if (j < 10) {
                setPixelsColor(R.color.color_princess_6, 50, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_3, 50, j)
            } else if (j < 14) {
                setPixelsColor(R.color.color_princess_6, 50, j)
            } else if (j < 16) {
                setPixelsColor(R.color.color_princess_10, 50, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_9, 50, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_10, 50, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_5, 50, j)
            } else if (j < 25) {
                setPixelsColor(R.color.color_princess_2, 50, j)
            } else if (j < 28) {
                setPixelsColor(R.color.color_princess_4, 50, j)
            } else if (j < 31) {
                setPixelsColor(R.color.color_princess_2, 50, j)
            } else if (j < 36) {
                setPixelsColor(R.color.color_princess_9, 50, j)
            } else if (j < 37) {
                setPixelsColor(R.color.color_princess_10, 50, j)
            } else if (j < 38) {
                setPixelsColor(R.color.color_princess_9, 50, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 50, j)
            } else if (j < 44) {
                setPixelsColor(R.color.color_princess_10, 50, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_8, 50, j)
            } else if (j < 48) {
                setPixelsColor(R.color.color_princess_12, 50, j)
            } else if (j < 49) {
                setPixelsColor(R.color.color_princess_11, 50, j)
            } else if (j < 51) {
                setPixelsColor(R.color.color_princess_9, 50, j)
            } else if (j < 52) {
                setPixelsColor(R.color.color_princess_2, 50, j)
            } else if (j < 66) {
                setPixelsColor(R.color.color_princess_6, 50, j)
            } else if (j < 67) {
                setPixelsColor(R.color.color_princess_7, 50, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 50, j)
            }
        }
    }


    /**
     * 绘制第50列
     */
    private fun draw50Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[49][j]
            if (j < 7) {
                setPixelsColor(R.color.color_princess_6, 49, j)
            } else if (j < 9) {
                setPixelsColor(R.color.color_princess_5, 49, j)
            } else if (j < 13) {
                setPixelsColor(R.color.color_princess_3, 49, j)
            } else if (j < 14) {
                setPixelsColor(R.color.color_princess_5, 49, j)
            } else if (j < 15) {
                setPixelsColor(R.color.color_princess_6, 49, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_10, 49, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_9, 49, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_10, 49, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_5, 49, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_10, 49, j)
            } else if (j < 25) {
                setPixelsColor(R.color.color_princess_2, 49, j)
            } else if (j < 29) {
                setPixelsColor(R.color.color_princess_4, 49, j)
            } else if (j < 32) {
                setPixelsColor(R.color.color_princess_2, 49, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_9, 49, j)
            } else if (j < 35) {
                setPixelsColor(R.color.color_princess_11, 49, j)
            } else if (j < 37) {
                setPixelsColor(R.color.color_princess_10, 49, j)
            } else if (j < 38) {
                setPixelsColor(R.color.color_princess_9, 49, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 49, j)
            } else if (j < 44) {
                setPixelsColor(R.color.color_princess_11, 49, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_5, 49, j)
            } else if (j < 47) {
                setPixelsColor(R.color.color_princess_12, 49, j)
            } else if (j < 48) {
                setPixelsColor(R.color.color_princess_11, 49, j)
            } else if (j < 49) {
                setPixelsColor(R.color.color_princess_10, 49, j)
            } else if (j < 51) {
                setPixelsColor(R.color.color_princess_9, 49, j)
            } else if (j < 53) {
                setPixelsColor(R.color.color_princess_2, 49, j)
            } else if (j < 54) {
                setPixelsColor(R.color.color_princess_9, 49, j)
            } else if (j < 65) {
                setPixelsColor(R.color.color_princess_6, 49, j)
            } else if (j < 66) {
                setPixelsColor(R.color.color_princess_7, 49, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 49, j)
            }
        }
    }


    /**
     * 绘制第49列
     */
    private fun draw49Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[48][j]
            if (j < 8) {
                setPixelsColor(R.color.color_princess_6, 48, j)
            } else if (j < 9) {
                setPixelsColor(R.color.color_princess_5, 48, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_3, 48, j)
            } else if (j < 15) {
                setPixelsColor(R.color.color_princess_5, 48, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_9, 48, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_6, 48, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_5, 48, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_9, 48, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_10, 48, j)
            } else if (j < 25) {
                setPixelsColor(R.color.color_princess_2, 48, j)
            } else if (j < 31) {
                setPixelsColor(R.color.color_princess_4, 48, j)
            } else if (j < 33) {
                setPixelsColor(R.color.color_princess_2, 48, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_9, 48, j)
            } else if (j < 37) {
                setPixelsColor(R.color.color_princess_10, 48, j)
            } else if (j < 38) {
                setPixelsColor(R.color.color_princess_9, 48, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 48, j)
            } else if (j < 44) {
                setPixelsColor(R.color.color_princess_11, 48, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_5, 48, j)
            } else if (j < 46) {
                setPixelsColor(R.color.color_princess_11, 48, j)
            } else if (j < 47) {
                setPixelsColor(R.color.color_princess_10, 48, j)
            } else if (j < 48) {
                setPixelsColor(R.color.color_princess_11, 48, j)
            } else if (j < 49) {
                setPixelsColor(R.color.color_princess_10, 48, j)
            } else if (j < 51) {
                setPixelsColor(R.color.color_princess_9, 48, j)
            } else if (j < 54) {
                setPixelsColor(R.color.color_princess_2, 48, j)
            } else if (j < 64) {
                setPixelsColor(R.color.color_princess_6, 48, j)
            } else if (j < 66) {
                setPixelsColor(R.color.color_princess_9, 48, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 48, j)
            }
        }
    }


    /**
     * 绘制第48列
     */
    private fun draw48Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[47][j]
            if (j < 9) {
                setPixelsColor(R.color.color_princess_6, 47, j)
            } else if (j < 11) {
                setPixelsColor(R.color.color_princess_3, 47, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_5, 47, j)
            } else if (j < 13) {
                setPixelsColor(R.color.color_princess_6, 47, j)
            } else if (j < 14) {
                setPixelsColor(R.color.color_princess_5, 47, j)
            } else if (j < 16) {
                setPixelsColor(R.color.color_princess_6, 47, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_10, 47, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_9, 47, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_5, 47, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_9, 47, j)
            } else if (j < 26) {
                setPixelsColor(R.color.color_princess_2, 47, j)
            } else if (j < 31) {
                setPixelsColor(R.color.color_princess_4, 47, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_2, 47, j)
            } else if (j < 37) {
                setPixelsColor(R.color.color_princess_9, 47, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 47, j)
            } else if (j < 44) {
                setPixelsColor(R.color.color_princess_9, 47, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_5, 47, j)
            } else if (j < 49) {
                setPixelsColor(R.color.color_princess_10, 47, j)
            } else if (j < 52) {
                setPixelsColor(R.color.color_princess_9, 47, j)
            } else if (j < 54) {
                setPixelsColor(R.color.color_princess_2, 47, j)
            } else if (j < 55) {
                setPixelsColor(R.color.color_princess_9, 47, j)
            } else if (j < 63) {
                setPixelsColor(R.color.color_princess_6, 47, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 47, j)
            }
        }
    }


    /**
     * 绘制第47列
     */
    private fun draw47Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[46][j]
            if (j < 4) {
                setPixelsColor(R.color.color_princess_6, 46, j)
            } else if (j < 5) {
                setPixelsColor(R.color.color_princess_5, 46, j)
            } else if (j < 9) {
                setPixelsColor(R.color.color_princess_6, 46, j)
            } else if (j < 11) {
                setPixelsColor(R.color.color_princess_5, 46, j)
            } else if (j < 15) {
                setPixelsColor(R.color.color_princess_6, 46, j)
            } else if (j < 16) {
                setPixelsColor(R.color.color_princess_5, 46, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_10, 46, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_3, 46, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_6, 46, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_9, 46, j)
            } else if (j < 24) {
                setPixelsColor(R.color.color_princess_2, 46, j)
            } else if (j < 33) {
                setPixelsColor(R.color.color_princess_4, 46, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 46, j)
            } else if (j < 44) {
                setPixelsColor(R.color.color_princess_9, 46, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_5, 46, j)
            } else if (j < 49) {
                setPixelsColor(R.color.color_princess_10, 46, j)
            } else if (j < 52) {
                setPixelsColor(R.color.color_princess_9, 46, j)
            } else if (j < 55) {
                setPixelsColor(R.color.color_princess_2, 46, j)
            } else if (j < 56) {
                setPixelsColor(R.color.color_princess_7, 46, j)
            } else if (j < 62) {
                setPixelsColor(R.color.color_princess_6, 46, j)
            } else if (j < 63) {
                setPixelsColor(R.color.color_princess_7, 46, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 46, j)
            }
        }
    }


    /**
     * 绘制第46列
     */
    private fun draw46Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[45][j]
            if (j < 5) {
                setPixelsColor(R.color.color_princess_6, 45, j)
            } else if (j < 6) {
                setPixelsColor(R.color.color_princess_5, 45, j)
            } else if (j < 9) {
                setPixelsColor(R.color.color_princess_6, 45, j)
            } else if (j < 11) {
                setPixelsColor(R.color.color_princess_5, 45, j)
            } else if (j < 13) {
                setPixelsColor(R.color.color_princess_6, 45, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_10, 45, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_5, 45, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_3, 45, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_5, 45, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_9, 45, j)
            } else if (j < 24) {
                setPixelsColor(R.color.color_princess_2, 45, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_4, 45, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 45, j)
            } else if (j < 44) {
                setPixelsColor(R.color.color_princess_9, 45, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_5, 45, j)
            } else if (j < 48) {
                setPixelsColor(R.color.color_princess_10, 45, j)
            } else if (j < 53) {
                setPixelsColor(R.color.color_princess_9, 45, j)
            } else if (j < 54) {
                setPixelsColor(R.color.color_princess_2, 45, j)
            } else if (j < 57) {
                setPixelsColor(R.color.color_princess_9, 45, j)
            } else if (j < 59) {
                setPixelsColor(R.color.color_princess_6, 45, j)
            } else if (j < 62) {
                setPixelsColor(R.color.color_princess_6, 45, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 45, j)
            }
        }
    }


    /**
     * 绘制第45列
     */
    private fun draw45Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[44][j]
            if (j < 1) {
                setPixelsColor(R.color.color_princess_6, 44, j)
            } else if (j < 2) {
                setPixelsColor(R.color.color_princess_5, 44, j)
            } else if (j < 3) {
                setPixelsColor(R.color.color_princess_3, 44, j)
            } else if (j < 9) {
                setPixelsColor(R.color.color_princess_6, 44, j)
            } else if (j < 10) {
                setPixelsColor(R.color.color_princess_5, 44, j)
            } else if (j < 11) {
                setPixelsColor(R.color.color_princess_6, 44, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_10, 44, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_5, 44, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_3, 44, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_9, 44, j)
            } else if (j < 24) {
                setPixelsColor(R.color.color_princess_2, 44, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_4, 44, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 44, j)
            } else if (j < 44) {
                setPixelsColor(R.color.color_princess_9, 44, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_10, 44, j)
            } else if (j < 49) {
                setPixelsColor(R.color.color_princess_9, 44, j)
            } else if (j < 50) {
                setPixelsColor(R.color.color_princess_10, 44, j)
            } else if (j < 57) {
                setPixelsColor(R.color.color_princess_9, 44, j)
            } else if (j < 59) {
                setPixelsColor(R.color.color_princess_6, 44, j)
            } else if (j < 61) {
                setPixelsColor(R.color.color_princess_6, 44, j)
            } else if (j < 62) {
                setPixelsColor(R.color.color_princess_9, 44, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 44, j)
            }
        }
    }


    /**
     * 绘制第44列
     */
    private fun draw44Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[43][j]
            if (j < 1) {
                setPixelsColor(R.color.color_princess_3, 43, j)
            } else if (j < 2) {
                setPixelsColor(R.color.color_princess_5, 43, j)
            } else if (j < 3) {
                setPixelsColor(R.color.color_princess_6, 43, j)
            } else if (j < 4) {
                setPixelsColor(R.color.color_princess_5, 43, j)
            } else if (j < 9) {
                setPixelsColor(R.color.color_princess_6, 43, j)
            } else if (j < 10) {
                setPixelsColor(R.color.color_princess_5, 43, j)
            } else if (j < 11) {
                setPixelsColor(R.color.color_princess_6, 43, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_9, 43, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_10, 43, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_5, 43, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_3, 43, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_6, 43, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_10, 43, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_9, 43, j)
            } else if (j < 25) {
                setPixelsColor(R.color.color_princess_2, 43, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_4, 43, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 43, j)
            } else if (j < 49) {
                setPixelsColor(R.color.color_princess_9, 43, j)
            } else if (j < 50) {
                setPixelsColor(R.color.color_princess_10, 43, j)
            } else if (j < 57) {
                setPixelsColor(R.color.color_princess_9, 43, j)
            } else if (j < 61) {
                setPixelsColor(R.color.color_princess_6, 43, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 43, j)
            }
        }
    }


    /**
     * 绘制第43列
     */
    private fun draw43Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[42][j]
            if (j < 2) {
                setPixelsColor(R.color.color_princess_5, 42, j)
            } else if (j < 6) {
                setPixelsColor(R.color.color_princess_6, 42, j)
            } else if (j < 7) {
                setPixelsColor(R.color.color_princess_5, 42, j)
            } else if (j < 9) {
                setPixelsColor(R.color.color_princess_6, 42, j)
            } else if (j < 10) {
                setPixelsColor(R.color.color_princess_5, 42, j)
            } else if (j < 11) {
                setPixelsColor(R.color.color_princess_6, 42, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_10, 42, j)
            } else if (j < 13) {
                setPixelsColor(R.color.color_princess_9, 42, j)
            } else if (j < 16) {
                setPixelsColor(R.color.color_princess_10, 42, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_5, 42, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_10, 42, j)
            } else if (j < 25) {
                setPixelsColor(R.color.color_princess_2, 42, j)
            } else if (j < 32) {
                setPixelsColor(R.color.color_princess_4, 42, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 42, j)
            } else if (j < 55) {
                setPixelsColor(R.color.color_princess_9, 42, j)
            } else if (j < 56) {
                setPixelsColor(R.color.color_princess_9, 42, j)
            } else if (j < 60) {
                setPixelsColor(R.color.color_princess_6, 42, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 42, j)
            }
        }
    }


    /**
     * 绘制第42列
     */
    private fun draw42Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[41][j]
            if (j < 2) {
                setPixelsColor(R.color.color_princess_6, 41, j)
            } else if (j < 5) {
                setPixelsColor(R.color.color_princess_5, 41, j)
            } else if (j < 7) {
                setPixelsColor(R.color.color_princess_6, 41, j)
            } else if (j < 8) {
                setPixelsColor(R.color.color_princess_3, 41, j)
            } else if (j < 9) {
                setPixelsColor(R.color.color_princess_6, 41, j)
            } else if (j < 10) {
                setPixelsColor(R.color.color_princess_5, 41, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_6, 41, j)
            } else if (j < 13) {
                setPixelsColor(R.color.color_princess_10, 41, j)
            } else if (j < 14) {
                setPixelsColor(R.color.color_princess_9, 41, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_10, 41, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_5, 41, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_10, 41, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_11, 41, j)
            } else if (j < 25) {
                setPixelsColor(R.color.color_princess_2, 41, j)
            } else if (j < 29) {
                setPixelsColor(R.color.color_princess_4, 41, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 41, j)
            } else if (j < 55) {
                setPixelsColor(R.color.color_princess_9, 41, j)
            } else if (j < 56) {
                setPixelsColor(R.color.color_princess_10, 41, j)
            } else if (j < 59) {
                setPixelsColor(R.color.color_princess_6, 41, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 41, j)
            }
        }
    }


    /**
     * 绘制第41列
     */
    private fun draw41Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[40][j]
            if (j < 2) {
                setPixelsColor(R.color.color_princess_6, 40, j)
            } else if (j < 4) {
                setPixelsColor(R.color.color_princess_5, 40, j)
            } else if (j < 6) {
                setPixelsColor(R.color.color_princess_3, 40, j)
            } else if (j < 7) {
                setPixelsColor(R.color.color_princess_5, 40, j)
            } else if (j < 8) {
                setPixelsColor(R.color.color_princess_3, 40, j)
            } else if (j < 11) {
                setPixelsColor(R.color.color_princess_5, 40, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_6, 40, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_10, 40, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_5, 40, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_10, 40, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_9, 40, j)
            } else if (j < 25) {
                setPixelsColor(R.color.color_princess_2, 40, j)
            } else if (j < 27) {
                setPixelsColor(R.color.color_princess_4, 40, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 40, j)
            } else if (j < 54) {
                setPixelsColor(R.color.color_princess_9, 40, j)
            } else if (j < 56) {
                setPixelsColor(R.color.color_princess_10, 40, j)
            } else if (j < 59) {
                setPixelsColor(R.color.color_princess_6, 40, j)
            } else if (j < 65) {
                setPixelsColor(R.color.color_princess_1, 40, j)
            } else if (j < 66) {
                setPixelsColor(R.color.color_princess_6, 40, j)
            } else if (j < 67) {
                setPixelsColor(R.color.color_princess_5, 40, j)
            } else if (j < 68) {
                setPixelsColor(R.color.color_princess_9, 40, j)
            } else if (j < 71) {
                setPixelsColor(R.color.color_princess_4, 40, j)
            } else if (j < 72) {
                setPixelsColor(R.color.color_princess_9, 40, j)
            } else if (j < 73) {
                setPixelsColor(R.color.color_princess_5, 40, j)
            } else if (j < 74) {
                setPixelsColor(R.color.color_princess_9, 40, j)
            } else if (j < 75) {
                setPixelsColor(R.color.color_princess_7, 40, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 40, j)
            }
        }
    }


    /**
     * 绘制第40列
     */
    private fun draw40Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[39][j]
            if (j < 2) {
                setPixelsColor(R.color.color_princess_3, 39, j)
            } else if (j < 3) {
                setPixelsColor(R.color.color_princess_5, 39, j)
            } else if (j < 4) {
                setPixelsColor(R.color.color_princess_6, 39, j)
            } else if (j < 5) {
                setPixelsColor(R.color.color_princess_5, 39, j)
            } else if (j < 6) {
                setPixelsColor(R.color.color_princess_6, 39, j)
            } else if (j < 7) {
                setPixelsColor(R.color.color_princess_5, 39, j)
            } else if (j < 11) {
                setPixelsColor(R.color.color_princess_3, 39, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_6, 39, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_10, 39, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_5, 39, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_10, 39, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_9, 39, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 39, j)
            } else if (j < 53) {
                setPixelsColor(R.color.color_princess_9, 39, j)
            } else if (j < 55) {
                setPixelsColor(R.color.color_princess_10, 39, j)
            } else if (j < 56) {
                setPixelsColor(R.color.color_princess_9, 39, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_6, 39, j)
            } else if (j < 60) {
                setPixelsColor(R.color.color_princess_1, 39, j)
            } else if (j < 61) {
                setPixelsColor(R.color.color_princess_9, 39, j)
            } else if (j < 62) {
                setPixelsColor(R.color.color_princess_5, 39, j)
            } else if (j < 63) {
                setPixelsColor(R.color.color_princess_10, 39, j)
            } else if (j < 64) {
                setPixelsColor(R.color.color_princess_9, 39, j)
            } else if (j < 66) {
                setPixelsColor(R.color.color_princess_2, 39, j)
            } else if (j < 74) {
                setPixelsColor(R.color.color_princess_4, 39, j)
            } else if (j < 75) {
                setPixelsColor(R.color.color_princess_9, 39, j)
            } else if (j < 76) {
                setPixelsColor(R.color.color_princess_6, 39, j)
            } else if (j < 77) {
                setPixelsColor(R.color.color_princess_5, 39, j)
            } else if (j < 78) {
                setPixelsColor(R.color.color_princess_9, 39, j)
            } else if (j < 79) {
                setPixelsColor(R.color.color_princess_7, 39, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 39, j)
            }
        }
    }


    /**
     * 绘制第39列
     */
    private fun draw39Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[38][j]
            if (j < 3) {
                setPixelsColor(R.color.color_princess_3, 38, j)
            } else if (j < 8) {
                setPixelsColor(R.color.color_princess_5, 38, j)
            } else if (j < 9) {
                setPixelsColor(R.color.color_princess_3, 38, j)
            } else if (j < 10) {
                setPixelsColor(R.color.color_princess_5, 38, j)
            } else if (j < 11) {
                setPixelsColor(R.color.color_princess_3, 38, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_5, 38, j)
            } else if (j < 13) {
                setPixelsColor(R.color.color_princess_6, 38, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_10, 38, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_9, 38, j)
            } else if (j < 44) {
                setPixelsColor(R.color.color_princess_2, 38, j)
            } else if (j < 53) {
                setPixelsColor(R.color.color_princess_9, 38, j)
            } else if (j < 55) {
                setPixelsColor(R.color.color_princess_10, 38, j)
            } else if (j < 56) {
                setPixelsColor(R.color.color_princess_9, 38, j)
            } else if (j < 57) {
                setPixelsColor(R.color.color_princess_6, 38, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_9, 38, j)
            } else if (j < 77) {
                setPixelsColor(R.color.color_princess_4, 38, j)
            } else if (j < 78) {
                setPixelsColor(R.color.color_princess_9, 38, j)
            } else if (j < 79) {
                setPixelsColor(R.color.color_princess_10, 38, j)
            } else if (j < 82) {
                setPixelsColor(R.color.color_princess_6, 38, j)
            } else if (j < 83) {
                setPixelsColor(R.color.color_princess_7, 38, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 38, j)
            }
        }
    }


    /**
     * 绘制第38列
     */
    private fun draw38Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[37][j]
            if (j < 4) {
                setPixelsColor(R.color.color_princess_3, 37, j)
            } else if (j < 9) {
                setPixelsColor(R.color.color_princess_5, 37, j)
            } else if (j < 10) {
                setPixelsColor(R.color.color_princess_6, 37, j)
            } else if (j < 11) {
                setPixelsColor(R.color.color_princess_5, 37, j)
            } else if (j < 13) {
                setPixelsColor(R.color.color_princess_3, 37, j)
            } else if (j < 14) {
                setPixelsColor(R.color.color_princess_6, 37, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_10, 37, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_9, 37, j)
            } else if (j < 38) {
                setPixelsColor(R.color.color_princess_2, 37, j)
            } else if (j < 40) {
                setPixelsColor(R.color.color_princess_9, 37, j)
            } else if (j < 42) {
                setPixelsColor(R.color.color_princess_2, 37, j)
            } else if (j < 51) {
                setPixelsColor(R.color.color_princess_9, 37, j)
            } else if (j < 55) {
                setPixelsColor(R.color.color_princess_10, 37, j)
            } else if (j < 57) {
                setPixelsColor(R.color.color_princess_6, 37, j)
            } else if (j < 60) {
                setPixelsColor(R.color.color_princess_2, 37, j)
            } else if (j < 81) {
                setPixelsColor(R.color.color_princess_4, 37, j)
            } else if (j < 82) {
                setPixelsColor(R.color.color_princess_9, 37, j)
            } else if (j < 84) {
                setPixelsColor(R.color.color_princess_10, 37, j)
            } else if (j < 86) {
                setPixelsColor(R.color.color_princess_5, 37, j)
            } else if (j < 87) {
                setPixelsColor(R.color.color_princess_6, 37, j)
            } else if (j < 88) {
                setPixelsColor(R.color.color_princess_9, 37, j)
            } else if (j < 89) {
                setPixelsColor(R.color.color_princess_6, 37, j)
            } else if (j < 90) {
                setPixelsColor(R.color.color_princess_7, 37, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 37, j)
            }
        }
    }


    /**
     * 绘制第37列
     */
    private fun draw37Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[36][j]
            if (j < 6) {
                setPixelsColor(R.color.color_princess_3, 36, j)
            } else if (j < 11) {
                setPixelsColor(R.color.color_princess_5, 36, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_3, 36, j)
            } else if (j < 14) {
                setPixelsColor(R.color.color_princess_5, 36, j)
            } else if (j < 15) {
                setPixelsColor(R.color.color_princess_6, 36, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_10, 36, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_9, 36, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_2, 36, j)
            } else if (j < 41) {
                setPixelsColor(R.color.color_princess_9, 36, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_2, 36, j)
            } else if (j < 50) {
                setPixelsColor(R.color.color_princess_9, 36, j)
            } else if (j < 55) {
                setPixelsColor(R.color.color_princess_10, 36, j)
            } else if (j < 56) {
                setPixelsColor(R.color.color_princess_9, 36, j)
            } else if (j < 62) {
                setPixelsColor(R.color.color_princess_2, 36, j)
            } else if (j < 85) {
                setPixelsColor(R.color.color_princess_4, 36, j)
            } else if (j < 87) {
                setPixelsColor(R.color.color_princess_9, 36, j)
            } else if (j < 88) {
                setPixelsColor(R.color.color_princess_10, 36, j)
            } else if (j < 91) {
                setPixelsColor(R.color.color_princess_5, 36, j)
            } else if (j < 93) {
                setPixelsColor(R.color.color_princess_6, 36, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 36, j)
            }
        }
    }


    /**
     * 绘制第36列
     */
    private fun draw36Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[35][j]
            if (j < 7) {
                setPixelsColor(R.color.color_princess_3, 35, j)
            } else if (j < 9) {
                setPixelsColor(R.color.color_princess_5, 35, j)
            } else if (j < 11) {
                setPixelsColor(R.color.color_princess_6, 35, j)
            } else if (j < 16) {
                setPixelsColor(R.color.color_princess_5, 35, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_10, 35, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_9, 35, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_2, 35, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_9, 35, j)
            } else if (j < 26) {
                setPixelsColor(R.color.color_princess_2, 35, j)
            } else if (j < 49) {
                setPixelsColor(R.color.color_princess_9, 35, j)
            } else if (j < 55) {
                setPixelsColor(R.color.color_princess_10, 35, j)
            } else if (j < 57) {
                setPixelsColor(R.color.color_princess_9, 35, j)
            } else if (j < 63) {
                setPixelsColor(R.color.color_princess_2, 35, j)
            } else if (j < 88) {
                setPixelsColor(R.color.color_princess_4, 35, j)
            } else if (j < 89) {
                setPixelsColor(R.color.color_princess_9, 35, j)
            } else if (j < 91) {
                setPixelsColor(R.color.color_princess_10, 35, j)
            } else if (j < 92) {
                setPixelsColor(R.color.color_princess_6, 35, j)
            } else if (j < 94) {
                setPixelsColor(R.color.color_princess_5, 35, j)
            } else if (j < 95) {
                setPixelsColor(R.color.color_princess_6, 35, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 35, j)
            }
        }
    }


    /**
     * 绘制第35列
     */
    private fun draw35Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[34][j]
            if (j < 9) {
                setPixelsColor(R.color.color_princess_3, 34, j)
            } else if (j < 15) {
                setPixelsColor(R.color.color_princess_5, 34, j)
            } else if (j < 16) {
                setPixelsColor(R.color.color_princess_6, 34, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_5, 34, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_10, 34, j)
            } else if (j < 48) {
                setPixelsColor(R.color.color_princess_9, 34, j)
            } else if (j < 56) {
                setPixelsColor(R.color.color_princess_10, 34, j)
            } else if (j < 57) {
                setPixelsColor(R.color.color_princess_9, 34, j)
            } else if (j < 64) {
                setPixelsColor(R.color.color_princess_2, 34, j)
            } else if (j < 90) {
                setPixelsColor(R.color.color_princess_4, 34, j)
            } else if (j < 92) {
                setPixelsColor(R.color.color_princess_9, 34, j)
            } else if (j < 94) {
                setPixelsColor(R.color.color_princess_10, 34, j)
            } else if (j < 95) {
                setPixelsColor(R.color.color_princess_5, 34, j)
            } else {
                setPixelsColor(R.color.color_princess_6, 34, j)
            }
        }
    }


    /**
     * 绘制第34列
     */
    private fun draw34Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[33][j]
            if (j < 10) {
                setPixelsColor(R.color.color_princess_3, 33, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_6, 33, j)
            } else if (j < 13) {
                setPixelsColor(R.color.color_princess_10, 33, j)
            } else if (j < 14) {
                setPixelsColor(R.color.color_princess_5, 33, j)
            } else if (j < 15) {
                setPixelsColor(R.color.color_princess_6, 33, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_5, 33, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_10, 33, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_5, 33, j)
            } else if (j < 47) {
                setPixelsColor(R.color.color_princess_9, 33, j)
            } else if (j < 57) {
                setPixelsColor(R.color.color_princess_10, 33, j)
            } else if (j < 59) {
                setPixelsColor(R.color.color_princess_9, 33, j)
            } else if (j < 66) {
                setPixelsColor(R.color.color_princess_2, 33, j)
            } else if (j < 92) {
                setPixelsColor(R.color.color_princess_4, 33, j)
            } else if (j < 94) {
                setPixelsColor(R.color.color_princess_9, 33, j)
            } else if (j < 95) {
                setPixelsColor(R.color.color_princess_10, 33, j)
            } else {
                setPixelsColor(R.color.color_princess_6, 33, j)
            }
        }
    }


    /**
     * 绘制第33列
     */
    private fun draw33Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[32][j]
            if (j < 10) {
                setPixelsColor(R.color.color_princess_3, 32, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_5, 32, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_10, 32, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_5, 32, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_10, 32, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_9, 32, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_10, 32, j)
            } else if (j < 59) {
                setPixelsColor(R.color.color_princess_9, 32, j)
            } else if (j < 66) {
                setPixelsColor(R.color.color_princess_2, 32, j)
            } else if (j < 94) {
                setPixelsColor(R.color.color_princess_4, 32, j)
            } else {
                setPixelsColor(R.color.color_princess_9, 32, j)
            }
        }
    }


    /**
     * 绘制第32列
     */
    private fun draw32Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[31][j]
            if (j < 13) {
                setPixelsColor(R.color.color_princess_3, 31, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_10, 31, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_5, 31, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_10, 31, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_5, 31, j)
            } else if (j < 31) {
                setPixelsColor(R.color.color_princess_10, 31, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_9, 31, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_10, 31, j)
            } else if (j < 60) {
                setPixelsColor(R.color.color_princess_9, 31, j)
            } else if (j < 67) {
                setPixelsColor(R.color.color_princess_2, 31, j)
            } else {
                setPixelsColor(R.color.color_princess_4, 31, j)
            }
        }
    }


    /**
     * 绘制第31列
     */
    private fun draw31Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[30][j]
            if (j < 9) {
                setPixelsColor(R.color.color_princess_3, 30, j)
            } else if (j < 11) {
                setPixelsColor(R.color.color_princess_5, 30, j)
            } else if (j < 13) {
                setPixelsColor(R.color.color_princess_3, 30, j)
            } else if (j < 15) {
                setPixelsColor(R.color.color_princess_5, 30, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_10, 30, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_5, 30, j)
            } else if (j < 38) {
                setPixelsColor(R.color.color_princess_10, 30, j)
            } else if (j < 40) {
                setPixelsColor(R.color.color_princess_9, 30, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_10, 30, j)
            } else if (j < 61) {
                setPixelsColor(R.color.color_princess_9, 30, j)
            } else if (j < 68) {
                setPixelsColor(R.color.color_princess_2, 30, j)
            } else {
                setPixelsColor(R.color.color_princess_4, 30, j)
            }
        }
    }


    /**
     * 绘制第30列
     */
    private fun draw30Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[29][j]
            if (j < 9) {
                setPixelsColor(R.color.color_princess_3, 29, j)
            } else if (j < 12) {
                setPixelsColor(R.color.color_princess_5, 29, j)
            } else if (j < 14) {
                setPixelsColor(R.color.color_princess_3, 29, j)
            } else if (j < 16) {
                setPixelsColor(R.color.color_princess_5, 29, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_10, 29, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_5, 29, j)
            } else if (j < 24) {
                setPixelsColor(R.color.color_princess_3, 29, j)
            } else if (j < 26) {
                setPixelsColor(R.color.color_princess_5, 29, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_10, 29, j)
            } else if (j < 61) {
                setPixelsColor(R.color.color_princess_9, 29, j)
            } else if (j < 69) {
                setPixelsColor(R.color.color_princess_2, 29, j)
            } else {
                setPixelsColor(R.color.color_princess_4, 29, j)
            }
        }
    }


    /**
     * 绘制第29列
     */
    private fun draw29Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[28][j]
            if (j < 10) {
                setPixelsColor(R.color.color_princess_3, 28, j)
            } else if (j < 14) {
                setPixelsColor(R.color.color_princess_5, 28, j)
            } else if (j < 15) {
                setPixelsColor(R.color.color_princess_3, 28, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_5, 28, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_10, 28, j)
            } else if (j < 27) {
                setPixelsColor(R.color.color_princess_5, 28, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_10, 28, j)
            } else if (j < 62) {
                setPixelsColor(R.color.color_princess_9, 28, j)
            } else if (j < 70) {
                setPixelsColor(R.color.color_princess_2, 28, j)
            } else {
                setPixelsColor(R.color.color_princess_4, 28, j)
            }
        }
    }


    /**
     * 绘制第28列
     */
    private fun draw28Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[27][j]
            if (j < 12) {
                setPixelsColor(R.color.color_princess_3, 27, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_5, 27, j)
            } else if (j < 26) {
                setPixelsColor(R.color.color_princess_10, 27, j)
            } else if (j < 29) {
                setPixelsColor(R.color.color_princess_3, 27, j)
            } else if (j < 31) {
                setPixelsColor(R.color.color_princess_5, 27, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_10, 27, j)
            } else if (j < 62) {
                setPixelsColor(R.color.color_princess_9, 27, j)
            } else if (j < 70) {
                setPixelsColor(R.color.color_princess_2, 27, j)
            } else {
                setPixelsColor(R.color.color_princess_4, 27, j)
            }
        }
    }


    /**
     * 绘制第27列
     */
    private fun draw27Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[26][j]
            if (j < 12) {
                setPixelsColor(R.color.color_princess_3, 26, j)
            } else if (j < 17) {
                setPixelsColor(R.color.color_princess_5, 26, j)
            } else if (j < 18) {
                setPixelsColor(R.color.color_princess_3, 26, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_5, 26, j)
            } else if (j < 26) {
                setPixelsColor(R.color.color_princess_10, 26, j)
            } else if (j < 29) {
                setPixelsColor(R.color.color_princess_5, 26, j)
            } else if (j < 30) {
                setPixelsColor(R.color.color_princess_3, 26, j)
            } else if (j < 33) {
                setPixelsColor(R.color.color_princess_5, 26, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_10, 26, j)
            } else if (j < 44) {
                setPixelsColor(R.color.color_princess_5, 26, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_10, 26, j)
            } else if (j < 46) {
                setPixelsColor(R.color.color_princess_5, 26, j)
            } else if (j < 50) {
                setPixelsColor(R.color.color_princess_10, 26, j)
            } else if (j < 51) {
                setPixelsColor(R.color.color_princess_5, 26, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_10, 26, j)
            } else if (j < 62) {
                setPixelsColor(R.color.color_princess_9, 26, j)
            } else if (j < 70) {
                setPixelsColor(R.color.color_princess_2, 26, j)
            } else {
                setPixelsColor(R.color.color_princess_4, 26, j)
            }
        }
    }


    /**
     * 绘制第26列
     */
    private fun draw26Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[25][j]
            if (j < 15) {
                setPixelsColor(R.color.color_princess_3, 25, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_5, 25, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_3, 25, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_5, 25, j)
            } else if (j < 26) {
                setPixelsColor(R.color.color_princess_10, 25, j)
            } else if (j < 28) {
                setPixelsColor(R.color.color_princess_5, 25, j)
            } else if (j < 30) {
                setPixelsColor(R.color.color_princess_10, 25, j)
            } else if (j < 33) {
                setPixelsColor(R.color.color_princess_5, 25, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_3, 25, j)
            } else if (j < 38) {
                setPixelsColor(R.color.color_princess_5, 25, j)
            } else if (j < 39) {
                setPixelsColor(R.color.color_princess_10, 25, j)
            } else if (j < 40) {
                setPixelsColor(R.color.color_princess_5, 25, j)
            } else if (j < 41) {
                setPixelsColor(R.color.color_princess_10, 25, j)
            } else if (j < 42) {
                setPixelsColor(R.color.color_princess_5, 25, j)
            } else if (j < 44) {
                setPixelsColor(R.color.color_princess_6, 25, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_5, 25, j)
            } else if (j < 46) {
                setPixelsColor(R.color.color_princess_3, 25, j)
            } else if (j < 47) {
                setPixelsColor(R.color.color_princess_5, 25, j)
            } else if (j < 57) {
                setPixelsColor(R.color.color_princess_10, 25, j)
            } else if (j < 61) {
                setPixelsColor(R.color.color_princess_9, 25, j)
            } else if (j < 71) {
                setPixelsColor(R.color.color_princess_2, 25, j)
            } else {
                setPixelsColor(R.color.color_princess_4, 25, j)
            }
        }
    }


    /**
     * 绘制第25列
     */
    private fun draw25Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[24][j]
            if (j < 17) {
                setPixelsColor(R.color.color_princess_3, 24, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_5, 24, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_3, 24, j)
            } else if (j < 31) {
                setPixelsColor(R.color.color_princess_5, 24, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_10, 24, j)
            } else if (j < 35) {
                setPixelsColor(R.color.color_princess_5, 24, j)
            } else if (j < 36) {
                setPixelsColor(R.color.color_princess_10, 24, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_5, 24, j)
            } else if (j < 56) {
                setPixelsColor(R.color.color_princess_10, 24, j)
            } else if (j < 61) {
                setPixelsColor(R.color.color_princess_9, 24, j)
            } else if (j < 71) {
                setPixelsColor(R.color.color_princess_2, 24, j)
            } else {
                setPixelsColor(R.color.color_princess_4, 24, j)
            }
        }
    }


    /**
     * 绘制第24列
     */
    private fun draw24Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[23][j]
            if (j < 18) {
                setPixelsColor(R.color.color_princess_3, 23, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_5, 23, j)
            } else if (j < 24) {
                setPixelsColor(R.color.color_princess_3, 23, j)
            } else if (j < 36) {
                setPixelsColor(R.color.color_princess_5, 23, j)
            } else if (j < 37) {
                setPixelsColor(R.color.color_princess_10, 23, j)
            } else if (j < 41) {
                setPixelsColor(R.color.color_princess_5, 23, j)
            } else if (j < 56) {
                setPixelsColor(R.color.color_princess_10, 23, j)
            } else if (j < 60) {
                setPixelsColor(R.color.color_princess_9, 23, j)
            } else if (j < 69) {
                setPixelsColor(R.color.color_princess_2, 23, j)
            } else {
                setPixelsColor(R.color.color_princess_4, 23, j)
            }
        }
    }


    /**
     * 绘制第23列
     */
    private fun draw23Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[22][j]
            if (j < 19) {
                setPixelsColor(R.color.color_princess_3, 22, j)
            } else if (j < 40) {
                setPixelsColor(R.color.color_princess_5, 22, j)
            } else if (j < 55) {
                setPixelsColor(R.color.color_princess_10, 22, j)
            } else if (j < 59) {
                setPixelsColor(R.color.color_princess_9, 22, j)
            } else if (j < 69) {
                setPixelsColor(R.color.color_princess_2, 22, j)
            } else {
                setPixelsColor(R.color.color_princess_4, 22, j)
            }
        }
    }


    /**
     * 绘制第22列
     */
    private fun draw22Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[21][j]
            if (j < 19) {
                setPixelsColor(R.color.color_princess_3, 21, j)
            } else if (j < 40) {
                setPixelsColor(R.color.color_princess_5, 21, j)
            } else if (j < 54) {
                setPixelsColor(R.color.color_princess_10, 21, j)
            } else if (j < 60) {
                setPixelsColor(R.color.color_princess_9, 21, j)
            } else if (j < 71) {
                setPixelsColor(R.color.color_princess_2, 21, j)
            } else {
                setPixelsColor(R.color.color_princess_4, 21, j)
            }
        }
    }


    /**
     * 绘制第21列
     */
    private fun draw21Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[20][j]
            if (j < 18) {
                setPixelsColor(R.color.color_princess_3, 20, j)
            } else if (j < 24) {
                setPixelsColor(R.color.color_princess_5, 20, j)
            } else if (j < 25) {
                setPixelsColor(R.color.color_princess_10, 20, j)
            } else if (j < 26) {
                setPixelsColor(R.color.color_princess_5, 20, j)
            } else if (j < 27) {
                setPixelsColor(R.color.color_princess_10, 20, j)
            } else if (j < 31) {
                setPixelsColor(R.color.color_princess_5, 20, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_10, 20, j)
            } else if (j < 39) {
                setPixelsColor(R.color.color_princess_5, 20, j)
            } else if (j < 54) {
                setPixelsColor(R.color.color_princess_10, 20, j)
            } else if (j < 65) {
                setPixelsColor(R.color.color_princess_9, 20, j)
            } else if (j < 73) {
                setPixelsColor(R.color.color_princess_2, 20, j)
            } else {
                setPixelsColor(R.color.color_princess_4, 20, j)
            }
        }
    }


    /**
     * 绘制第20列
     */
    private fun draw20Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[19][j]
            if (j < 18) {
                setPixelsColor(R.color.color_princess_3, 19, j)
            } else if (j < 28) {
                setPixelsColor(R.color.color_princess_5, 19, j)
            } else if (j < 30) {
                setPixelsColor(R.color.color_princess_10, 19, j)
            } else if (j < 38) {
                setPixelsColor(R.color.color_princess_5, 19, j)
            } else if (j < 40) {
                setPixelsColor(R.color.color_princess_10, 19, j)
            } else if (j < 55) {
                setPixelsColor(R.color.color_princess_10, 19, j)
            } else if (j < 70) {
                setPixelsColor(R.color.color_princess_9, 19, j)
            } else if (j < 73) {
                setPixelsColor(R.color.color_princess_2, 19, j)
            } else {
                setPixelsColor(R.color.color_princess_4, 19, j)
            }
        }
    }


    /**
     * 绘制第19列
     */
    private fun draw19Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[18][j]
            if (j < 17) {
                setPixelsColor(R.color.color_princess_3, 18, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_5, 18, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_10, 18, j)
            } else if (j < 24) {
                setPixelsColor(R.color.color_princess_5, 18, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_10, 18, j)
            } else if (j < 36) {
                setPixelsColor(R.color.color_princess_5, 18, j)
            } else if (j < 39) {
                setPixelsColor(R.color.color_princess_10, 18, j)
            } else if (j < 40) {
                setPixelsColor(R.color.color_princess_5, 18, j)
            } else if (j < 56) {
                setPixelsColor(R.color.color_princess_10, 18, j)
            } else if (j < 70) {
                setPixelsColor(R.color.color_princess_9, 18, j)
            } else if (j < 72) {
                setPixelsColor(R.color.color_princess_2, 18, j)
            } else {
                setPixelsColor(R.color.color_princess_4, 18, j)
            }
        }
    }


    /**
     * 绘制第18列
     */
    private fun draw18Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[17][j]
            if (j < 16) {
                setPixelsColor(R.color.color_princess_3, 17, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_5, 17, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_10, 17, j)
            } else if (j < 24) {
                setPixelsColor(R.color.color_princess_5, 17, j)
            } else if (j < 28) {
                setPixelsColor(R.color.color_princess_10, 17, j)
            } else if (j < 29) {
                setPixelsColor(R.color.color_princess_5, 17, j)
            } else if (j < 33) {
                setPixelsColor(R.color.color_princess_10, 17, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_5, 17, j)
            } else if (j < 40) {
                setPixelsColor(R.color.color_princess_10, 17, j)
            } else if (j < 57) {
                setPixelsColor(R.color.color_princess_10, 17, j)
            } else if (j < 70) {
                setPixelsColor(R.color.color_princess_9, 17, j)
            } else if (j < 71) {
                setPixelsColor(R.color.color_princess_2, 17, j)
            } else if (j < 94) {
                setPixelsColor(R.color.color_princess_4, 17, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 17, j)
            }
        }
    }


    /**
     * 绘制第17列
     */
    private fun draw17Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[16][j]
            if (j < 17) {
                setPixelsColor(R.color.color_princess_3, 16, j)
            } else if (j < 19) {
                setPixelsColor(R.color.color_princess_5, 16, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_10, 16, j)
            } else if (j < 25) {
                setPixelsColor(R.color.color_princess_5, 16, j)
            } else if (j < 30) {
                setPixelsColor(R.color.color_princess_10, 16, j)
            } else if (j < 34) {
                setPixelsColor(R.color.color_princess_5, 16, j)
            } else if (j < 35) {
                setPixelsColor(R.color.color_princess_3, 16, j)
            } else if (j < 36) {
                setPixelsColor(R.color.color_princess_11, 16, j)
            } else if (j < 37) {
                setPixelsColor(R.color.color_princess_10, 16, j)
            } else if (j < 39) {
                setPixelsColor(R.color.color_princess_5, 16, j)
            } else if (j < 40) {
                setPixelsColor(R.color.color_princess_10, 16, j)
            } else if (j < 46) {
                setPixelsColor(R.color.color_princess_6, 16, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_10, 16, j)
            } else if (j < 66) {
                setPixelsColor(R.color.color_princess_9, 16, j)
            } else if (j < 69) {
                setPixelsColor(R.color.color_princess_2, 16, j)
            } else if (j < 92) {
                setPixelsColor(R.color.color_princess_4, 16, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 16, j)
            }
        }
    }


    /**
     * 绘制第16列
     */
    private fun draw16Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[15][j]
            if (j < 18) {
                setPixelsColor(R.color.color_princess_3, 15, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_5, 15, j)
            } else if (j < 29) {
                setPixelsColor(R.color.color_princess_10, 15, j)
            } else if (j < 30) {
                setPixelsColor(R.color.color_princess_5, 15, j)
            } else if (j < 35) {
                setPixelsColor(R.color.color_princess_3, 15, j)
            } else if (j < 38) {
                setPixelsColor(R.color.color_princess_5, 15, j)
            } else if (j < 40) {
                setPixelsColor(R.color.color_princess_3, 15, j)
            } else if (j < 47) {
                setPixelsColor(R.color.color_princess_6, 15, j)
            } else if (j < 60) {
                setPixelsColor(R.color.color_princess_10, 15, j)
            } else if (j < 65) {
                setPixelsColor(R.color.color_princess_9, 15, j)
            } else if (j < 68) {
                setPixelsColor(R.color.color_princess_2, 15, j)
            } else if (j < 90) {
                setPixelsColor(R.color.color_princess_4, 15, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 15, j)
            }
        }
    }


    /**
     * 绘制第15列
     */
    private fun draw15Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[14][j]
            if (j < 21) {
                setPixelsColor(R.color.color_princess_3, 14, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_5, 14, j)
            } else if (j < 28) {
                setPixelsColor(R.color.color_princess_10, 14, j)
            } else if (j < 36) {
                setPixelsColor(R.color.color_princess_3, 14, j)
            } else if (j < 37) {
                setPixelsColor(R.color.color_princess_5, 14, j)
            } else if (j < 38) {
                setPixelsColor(R.color.color_princess_2, 14, j)
            } else if (j < 39) {
                setPixelsColor(R.color.color_princess_10, 14, j)
            } else if (j < 40) {
                setPixelsColor(R.color.color_princess_3, 14, j)
            } else if (j < 50) {
                setPixelsColor(R.color.color_princess_6, 14, j)
            } else if (j < 61) {
                setPixelsColor(R.color.color_princess_10, 14, j)
            } else if (j < 66) {
                setPixelsColor(R.color.color_princess_9, 14, j)
            } else if (j < 68) {
                setPixelsColor(R.color.color_princess_2, 14, j)
            } else if (j < 88) {
                setPixelsColor(R.color.color_princess_4, 14, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 14, j)
            }
        }
    }


    /**
     * 绘制第14列
     */
    private fun draw14Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[13][j]
            if (j < 36) {
                setPixelsColor(R.color.color_princess_3, 13, j)
            } else if (j < 42) {
                setPixelsColor(R.color.color_princess_13, 13, j)
            } else if (j < 50) {
                setPixelsColor(R.color.color_princess_6, 13, j)
            } else if (j < 64) {
                setPixelsColor(R.color.color_princess_10, 13, j)
            } else if (j < 67) {
                setPixelsColor(R.color.color_princess_9, 13, j)
            } else if (j < 69) {
                setPixelsColor(R.color.color_princess_2, 13, j)
            } else if (j < 86) {
                setPixelsColor(R.color.color_princess_4, 13, j)
            } else if (j < 93) {
                setPixelsColor(R.color.color_princess_1, 13, j)
            } else if (j < 95) {
                setPixelsColor(R.color.color_princess_7, 13, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 13, j)
            }
        }
    }


    /**
     * 绘制第13列
     */
    private fun draw13Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[12][j]
            if (j < 4) {
                setPixelsColor(R.color.color_princess_6, 12, j)
            } else if (j < 36) {
                setPixelsColor(R.color.color_princess_3, 12, j)
            } else if (j < 52) {
                setPixelsColor(R.color.color_princess_6, 12, j)
            } else if (j < 53) {
                setPixelsColor(R.color.color_princess_5, 12, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_10, 12, j)
            } else if (j < 64) {
                setPixelsColor(R.color.color_princess_9, 12, j)
            } else if (j < 65) {
                setPixelsColor(R.color.color_princess_10, 12, j)
            } else if (j < 68) {
                setPixelsColor(R.color.color_princess_9, 12, j)
            } else if (j < 70) {
                setPixelsColor(R.color.color_princess_2, 12, j)
            } else if (j < 83) {
                setPixelsColor(R.color.color_princess_4, 12, j)
            } else if (j < 90) {
                setPixelsColor(R.color.color_princess_1, 12, j)
            } else if (j < 92) {
                setPixelsColor(R.color.color_princess_7, 12, j)
            } else if (j < 95) {
                setPixelsColor(R.color.color_princess_1, 12, j)
            } else {
                setPixelsColor(R.color.color_princess_7, 12, j)
            }
        }
    }


    /**
     * 绘制第12列
     */
    private fun draw12Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[11][j]
            if (j < 7) {
                setPixelsColor(R.color.color_princess_6, 11, j)
            } else if (j < 31) {
                setPixelsColor(R.color.color_princess_3, 11, j)
            } else if (j < 53) {
                setPixelsColor(R.color.color_princess_6, 11, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_10, 11, j)
            } else if (j < 67) {
                setPixelsColor(R.color.color_princess_9, 11, j)
            } else if (j < 69) {
                setPixelsColor(R.color.color_princess_2, 11, j)
            } else if (j < 80) {
                setPixelsColor(R.color.color_princess_4, 11, j)
            } else if (j < 87) {
                setPixelsColor(R.color.color_princess_1, 11, j)
            } else if (j < 93) {
                setPixelsColor(R.color.color_princess_7, 11, j)
            } else if (j < 95) {
                setPixelsColor(R.color.color_princess_1, 11, j)
            } else {
                setPixelsColor(R.color.color_princess_7, 11, j)
            }
        }
    }


    /**
     * 绘制第11列
     */
    private fun draw11Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[10][j]
            if (j < 12) {
                setPixelsColor(R.color.color_princess_6, 10, j)
            } else if (j < 20) {
                setPixelsColor(R.color.color_princess_3, 10, j)
            } else if (j < 21) {
                setPixelsColor(R.color.color_princess_6, 10, j)
            } else if (j < 22) {
                setPixelsColor(R.color.color_princess_3, 10, j)
            } else if (j < 23) {
                setPixelsColor(R.color.color_princess_6, 10, j)
            } else if (j < 25) {
                setPixelsColor(R.color.color_princess_3, 10, j)
            } else if (j < 28) {
                setPixelsColor(R.color.color_princess_13, 10, j)
            } else if (j < 54) {
                setPixelsColor(R.color.color_princess_6, 10, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_10, 10, j)
            } else if (j < 67) {
                setPixelsColor(R.color.color_princess_9, 10, j)
            } else if (j < 69) {
                setPixelsColor(R.color.color_princess_2, 10, j)
            } else if (j < 78) {
                setPixelsColor(R.color.color_princess_4, 10, j)
            } else if (j < 80) {
                setPixelsColor(R.color.color_princess_1, 10, j)
            } else {
                setPixelsColor(R.color.color_princess_7, 10, j)
            }
        }
    }


    /**
     * 绘制第10列
     */
    private fun draw10Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[9][j]
            if (j < 15) {
                setPixelsColor(R.color.color_princess_6, 9, j)
            } else if (j < 26) {
                setPixelsColor(R.color.color_princess_3, 9, j)
            } else if (j < 54) {
                setPixelsColor(R.color.color_princess_6, 9, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_10, 9, j)
            } else if (j < 63) {
                setPixelsColor(R.color.color_princess_9, 9, j)
            } else if (j < 64) {
                setPixelsColor(R.color.color_princess_2, 9, j)
            } else if (j < 65) {
                setPixelsColor(R.color.color_princess_9, 9, j)
            } else if (j < 70) {
                setPixelsColor(R.color.color_princess_2, 9, j)
            } else if (j < 75) {
                setPixelsColor(R.color.color_princess_4, 9, j)
            } else {
                setPixelsColor(R.color.color_princess_7, 9, j)
            }
        }
    }


    /**
     * 绘制第9列
     */
    private fun draw9Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[8][j]
            if (j < 20) {
                setPixelsColor(R.color.color_princess_6, 8, j)
            } else if (j < 27) {
                setPixelsColor(R.color.color_princess_3, 8, j)
            } else if (j < 53) {
                setPixelsColor(R.color.color_princess_6, 8, j)
            } else if (j < 57) {
                setPixelsColor(R.color.color_princess_10, 8, j)
            } else if (j < 62) {
                setPixelsColor(R.color.color_princess_9, 8, j)
            } else if (j < 70) {
                setPixelsColor(R.color.color_princess_2, 8, j)
            } else if (j < 71) {
                setPixelsColor(R.color.color_princess_4, 8, j)
            } else if (j < 79) {
                setPixelsColor(R.color.color_princess_7, 8, j)
            } else if (j < 81) {
                setPixelsColor(R.color.color_princess_1, 8, j)
            } else if (j < 88) {
                setPixelsColor(R.color.color_princess_7, 8, j)
            } else if (j < 93) {
                setPixelsColor(R.color.color_princess_1, 8, j)
            } else {
                setPixelsColor(R.color.color_princess_7, 8, j)
            }
        }
    }


    /**
     * 绘制第8列
     */
    private fun draw8Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[7][j]
            if (j < 24) {
                setPixelsColor(R.color.color_princess_6, 7, j)
            } else if (j < 31) {
                setPixelsColor(R.color.color_princess_3, 7, j)
            } else if (j < 54) {
                setPixelsColor(R.color.color_princess_6, 7, j)
            } else if (j < 57) {
                setPixelsColor(R.color.color_princess_10, 7, j)
            } else if (j < 63) {
                setPixelsColor(R.color.color_princess_9, 7, j)
            } else if (j < 65) {
                setPixelsColor(R.color.color_princess_2, 7, j)
            } else if (j < 68) {
                setPixelsColor(R.color.color_princess_9, 7, j)
            } else if (j < 75) {
                setPixelsColor(R.color.color_princess_7, 7, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 7, j)
            }
        }
    }


    /**
     * 绘制第7列
     */
    private fun draw7Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[6][j]
            if (j < 28) {
                setPixelsColor(R.color.color_princess_6, 6, j)
            } else if (j < 35) {
                setPixelsColor(R.color.color_princess_3, 6, j)
            } else if (j < 54) {
                setPixelsColor(R.color.color_princess_6, 6, j)
            } else if (j < 56) {
                setPixelsColor(R.color.color_princess_10, 6, j)
            } else if (j < 66) {
                setPixelsColor(R.color.color_princess_9, 6, j)
            } else if (j < 70) {
                setPixelsColor(R.color.color_princess_7, 6, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 6, j)
            }
        }
    }


    /**
     * 绘制第6列
     */
    private fun draw6Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[5][j]
            if (j < 33) {
                setPixelsColor(R.color.color_princess_6, 5, j)
            } else if (j < 39) {
                setPixelsColor(R.color.color_princess_3, 5, j)
            } else if (j < 54) {
                setPixelsColor(R.color.color_princess_6, 5, j)
            } else if (j < 56) {
                setPixelsColor(R.color.color_princess_10, 5, j)
            } else if (j < 60) {
                setPixelsColor(R.color.color_princess_9, 5, j)
            } else if (j < 71) {
                setPixelsColor(R.color.color_princess_7, 5, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 5, j)
            }
        }
    }


    /**
     * 绘制第5列
     */
    private fun draw5Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[4][j]
            if (j < 36) {
                setPixelsColor(R.color.color_princess_6, 4, j)
            } else if (j < 43) {
                setPixelsColor(R.color.color_princess_3, 4, j)
            } else if (j < 45) {
                setPixelsColor(R.color.color_princess_6, 4, j)
            } else if (j < 50) {
                setPixelsColor(R.color.color_princess_13, 4, j)
            } else if (j < 55) {
                setPixelsColor(R.color.color_princess_6, 4, j)
            } else if (j < 56) {
                setPixelsColor(R.color.color_princess_10, 4, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_6, 4, j)
            } else if (j < 74) {
                setPixelsColor(R.color.color_princess_7, 4, j)
            } else {
                setPixelsColor(R.color.color_princess_1, 4, j)
            }
        }
    }


    /**
     * 绘制第4列
     */
    private fun draw4Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[3][j]
            if (j < 40) {
                setPixelsColor(R.color.color_princess_6, 3, j)
            } else if (j < 47) {
                setPixelsColor(R.color.color_princess_3, 3, j)
            } else if (j < 57) {
                setPixelsColor(R.color.color_princess_6, 3, j)
            } else {
                setPixelsColor(R.color.color_princess_7, 3, j)
            }
        }
    }

    /**
     * 绘制第3列
     */
    private fun draw3Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[2][j]
            if (j < 44) {
                setPixelsColor(R.color.color_princess_6, 2, j)
            } else if (j < 51) {
                setPixelsColor(R.color.color_princess_3, 2, j)
            } else if (j < 57) {
                setPixelsColor(R.color.color_princess_6, 2, j)
            } else {
                setPixelsColor(R.color.color_princess_7, 2, j)
            }
        }
    }


    /**
     * 绘制第2列
     */
    private fun draw2Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[1][j]
            if (j < 48) {
                setPixelsColor(R.color.color_princess_6, 1, j)
            } else if (j < 54) {
                setPixelsColor(R.color.color_princess_3, 1, j)
            } else if (j < 58) {
                setPixelsColor(R.color.color_princess_6, 1, j)
            } else {
                setPixelsColor(R.color.color_princess_7, 1, j)
            }
        }
    }


    /**
     * 绘制第1列
     */
    private fun draw1Column() {
        for (j in 0 until COLUMN_SIZE) {
            val coordinate = mCoordinateArray[0][j]
            if (j < 52) {
                setPixelsColor(R.color.color_princess_6, 0, j)
            } else if (j < 55) {
                setPixelsColor(R.color.color_princess_3, 0, j)
            } else if (j < 72) {
                setPixelsColor(R.color.color_princess_7, 0, j)
            } else if (j < 83) {
                setPixelsColor(R.color.color_princess_1, 0, j)
            } else {
                setPixelsColor(R.color.color_princess_7, 0, j)
            }
        }
    }


    /**
     * 绘制单个像素点
     */
    private fun setPixelsColor(color: Int, i: Int, j: Int) {
        mCoordinateArray[i][j].setColor(color)
    }


    /**
     * 输出日志信息
     */
    private fun showLog(msg: String) {
        Log.i(TAG, msg)
    }

}