package com.az.love.aq

import android.content.Intent
import android.os.Handler
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.az.love.aq.base.BaseAbstractActivity
import com.az.love.aq.service.MusicService
import com.az.love.aq.view.MyLittlePrincessView
import java.util.*

/**
 * @author lizheng.zhao
 * @date 2023/02/26
 *
 * @鸟都不知该往哪飞好了
 * @你的笑更像一些会飘的白云
 */
class MainActivity : BaseAbstractActivity() {

    override fun getLayoutId() = R.layout.activity_main

    companion object {

        private const val TIME_FALL_ANIM = 4000L

        const val TIME_W1_SHOW = TIME_FALL_ANIM + 3000L
        const val TIME_W2_SHOW = TIME_FALL_ANIM + 4000L


        const val SECONDS_300 = 300L
        const val SECONDS_2000 = 2000L
        const val SECONDS_3000 = 3000L
        const val SECONDS_4000 = 4000L
        const val SECONDS_5000 = 5000L
        const val SECONDS_6000 = 6000L
    }


    private lateinit var mImgBgW1: ImageView
    private lateinit var mImgBgW2: ImageView

    private lateinit var mImgBgFlower: ImageView
    private lateinit var mImgBgPrince: ImageView


    private lateinit var mPrincess: MyLittlePrincessView
    private lateinit var mPhotoFrameView: ImageView


    private lateinit var mImgBgFlowers: ImageView
    private lateinit var mImgBgAFlower: ImageView


    private lateinit var mImgBgLove1: ImageView
    private lateinit var mImgBgLove2: ImageView


    private lateinit var mAnimAlpha01: Animation
    private lateinit var mAnimAlpha10: Animation

    private lateinit var mAnimTranslateToDown: Animation
    private lateinit var mAnimTranslateToLeft: Animation
    private lateinit var mAnimTranslateToRight: Animation

    private lateinit var mAnimScaleToBig: Animation
    private lateinit var mAnimScaleTSmall: Animation
    private lateinit var mAnimScaleTSmall0: Animation


    private var mCount = 0


    override fun init() {
        startService(Intent(this, MusicService::class.java))
        initView()
    }

    private fun initView() {
        mImgBgW1 = findViewById(R.id.id_img_bg_w1)
        mImgBgW2 = findViewById(R.id.id_img_bg_w2)
        mImgBgFlower = findViewById(R.id.id_img_bg_flower)
        mImgBgPrince = findViewById(R.id.id_img_bg_prince)
        mPrincess = findViewById(R.id.id_my_princess)
        mPhotoFrameView = findViewById(R.id.id_photo_frame)

        mImgBgFlowers = findViewById(R.id.id_img_bg_flowers)
        mImgBgAFlower = findViewById(R.id.id_img_bg_a_flower)

        mImgBgLove1 = findViewById(R.id.id_img_bg_love_1)
        mImgBgLove2 = findViewById(R.id.id_img_bg_love_2)

        mAnimAlpha01 = AnimationUtils.loadAnimation(this, R.anim.alpha_0_1)
        mAnimAlpha10 = AnimationUtils.loadAnimation(this, R.anim.alpha_1_0)
        mAnimTranslateToDown =
            AnimationUtils.loadAnimation(this, R.anim.translate_to_bottom_anim)
        mAnimTranslateToRight =
            AnimationUtils.loadAnimation(this, R.anim.translate_to_right_anim)
        mAnimTranslateToLeft =
            AnimationUtils.loadAnimation(this, R.anim.translate_to_left_anim)

        mAnimScaleToBig = AnimationUtils.loadAnimation(this, R.anim.scale_to_big)
        mAnimScaleTSmall = AnimationUtils.loadAnimation(this, R.anim.scale_to_small)
        mAnimScaleTSmall0 = AnimationUtils.loadAnimation(this, R.anim.scale_to_small_0)

        dealWithTime()
    }


    private fun dealWithTime() {

        mImgBgW1.visibility = View.VISIBLE
        mImgBgW1.startAnimation(mAnimTranslateToDown)
        Handler().postDelayed({
            mImgBgW1.visibility = View.GONE
            mImgBgW2.visibility = View.VISIBLE
            mImgBgW2.startAnimation(mAnimTranslateToDown)
            Handler().postDelayed({
                mImgBgW2.visibility = View.GONE
                mImgBgPrince.visibility = View.VISIBLE
                mImgBgPrince.startAnimation(mAnimTranslateToRight)
                Handler().postDelayed({
                    mImgBgFlower.visibility = View.VISIBLE
                    mImgBgFlower.startAnimation(mAnimTranslateToLeft)
                    Handler().postDelayed({
//                        mPrincess.visibility = View.VISIBLE
//                        drawPhoto()

                        mImgBgLove1.visibility = View.VISIBLE
                        mImgBgFlowers.visibility = View.VISIBLE
                        mImgBgFlowers.startAnimation(mAnimAlpha01)

                        Handler().postDelayed({
                            mImgBgLove1.visibility = View.GONE
                            mImgBgFlowers.startAnimation(mAnimAlpha10)
                            Handler().postDelayed({
                                mImgBgFlowers.visibility = View.GONE
                            }, SECONDS_4000)
                            Handler().postDelayed({
                                mImgBgLove2.visibility = View.VISIBLE
                                mImgBgAFlower.visibility = View.VISIBLE
                                mImgBgAFlower.startAnimation(mAnimAlpha01)
                                mImgBgAFlower.startAnimation(mAnimScaleToBig)
                                Handler().postDelayed({
                                    mImgBgAFlower.startAnimation(mAnimScaleTSmall)
                                    Handler().postDelayed({
                                        mImgBgAFlower.visibility = View.GONE
                                    }, SECONDS_3000)
                                    mPrincess.visibility = View.VISIBLE
                                    drawPhoto()
                                }, SECONDS_4000)
                            }, SECONDS_2000)
                        }, SECONDS_6000)


                    }, SECONDS_6000)
                }, TIME_FALL_ANIM)
            }, TIME_W2_SHOW)
        }, TIME_W1_SHOW)
    }


    private fun drawPhoto() {
        val timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                if (mCount <= MyLittlePrincessView.MAX_COUNT) {
                    runOnUiThread {
                        if (mCount == 4) {
                            mImgBgLove2.visibility = View.GONE
                        }
                        mPrincess.setCount(mCount++)
                    }
                } else {
                    timer.cancel()
                    runOnUiThread {
                        showFrame()
                    }
                }
            }
        }, 0, SECONDS_300)
    }


    private fun showFrame() {
        Handler().postDelayed({
            mPrincess.startAnimation(mAnimScaleTSmall0)
            Handler().postDelayed(
                {
                    mPrincess.visibility = View.GONE
                    mPhotoFrameView.visibility = View.VISIBLE
                    mPhotoFrameView.startAnimation(mAnimScaleToBig)
                }, SECONDS_4000
            )
        }, SECONDS_3000)
    }


}