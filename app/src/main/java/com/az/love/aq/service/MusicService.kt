package com.az.love.aq.service

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import com.az.love.aq.R

class MusicService : Service() {
    private lateinit var mediaPlayer: MediaPlayer


    override fun onCreate() {
        super.onCreate()
        // 创建MediaPlayer对象
        mediaPlayer = MediaPlayer.create(this, R.raw.starfall)
    }

    override fun onDestroy() {
        super.onDestroy()

        mediaPlayer.stop()
        mediaPlayer.reset()
        // 释放MediaPlayer对象
        mediaPlayer.release()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        // 在这里执行需要执行的操作
        // 开始播放音频文件
        if (mediaPlayer != null && !mediaPlayer.isPlaying) {
            mediaPlayer.start()
        }
        return START_STICKY
    }

    override fun onBind(p0: Intent?) = null


}