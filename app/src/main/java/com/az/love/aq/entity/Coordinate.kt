package com.az.love.aq.entity

/**
 * @author lizheng.zhao
 * @date 2023/02/26
 *
 * @山不在高有你就行
 * @水不在深有你则灵
 * @似是爱情惟我不醒
 */
class Coordinate(
    private var x: Float = 0f,
    private var y: Float = 0f,
    private var sideLength: Float = 0f,
    private var color: Int = 0
) : java.io.Serializable {


    /**
     * 设置坐标值
     */
    fun setX(x: Float) {
        this.x = x
    }

    fun setY(y: Float) {
        this.y = y
    }


    /**
     * 设置边长
     */
    fun setSideLength(sideLength: Float) {
        this.sideLength = sideLength
    }


    fun setColor(color: Int) {
        this.color = color
    }

    fun getColor() = color


    /**
     * 左上角x, y坐标
     */
    fun getLeftTopX() = x
    fun getLeftTopY() = y

    /**
     * 右上角x, y坐标
     */
    fun getRightTopX() = x + sideLength
    fun getRightTopY() = y

    /**
     * 左下角x, y坐标
     */
    fun getLeftBottomX() = x
    fun getLeftBottomY() = y + sideLength

    /**
     * 右下角x, y坐标
     */
    fun getRightBottomX() = x + sideLength
    fun getRightBottomY() = y + sideLength

}
