package com.az.love.aq.base

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.az.love.aq.R
import pl.droidsonroids.gif.GifDrawable

/**
 * @author lizheng.zhao
 * @date 2023/02/26
 *
 * @你在阳光里
 * @我也在阳光里
 */
abstract class BaseAbstractActivity : AppCompatActivity() {


    private lateinit var mImgBgStar: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (getLayoutId() != 0) {
            val layoutInflater = LayoutInflater.from(this)
            val baseView = layoutInflater.inflate(getLayoutId(), null)
            setContentView(baseView)
            setImmersiveUIMode()
            mImgBgStar = findViewById(R.id.id_img_bg_star)
            val gifDrawable = GifDrawable(resources, R.drawable.bg_star)
            mImgBgStar.setImageDrawable(gifDrawable)
//            mImgBgStar.setOnClickListener {
//                val isHidden = isStatusBarNavigationBarHidden()
//                if (isHidden) {
//                    // 显示状态栏和导航栏
//                    window.decorView.systemUiVisibility =
//                        View.SYSTEM_UI_FLAG_VISIBLE or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
//                } else {
//                    // 隐藏状态栏和导航栏
//                    window.decorView.systemUiVisibility =
//                        View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                }
//            }
            init()
        }
    }


//    /**
//     * 判断状态栏和导航栏是否隐藏
//     */
//    private fun isStatusBarNavigationBarHidden(): Boolean {
//        // 获取 Window 对象
//        val window = window
//
//        // 获取根布局 View
//        val decorView = window.decorView
//
//        // 获取系统 UI 标志位
//        val uiFlags = decorView.systemUiVisibility
//
//        // 检查状态栏是否隐藏
//        val isStatusBarHidden = (uiFlags and View.SYSTEM_UI_FLAG_FULLSCREEN) != 0
//
//        // 检查导航栏是否隐藏
//        val isNavigationBarHidden = (uiFlags and View.SYSTEM_UI_FLAG_HIDE_NAVIGATION) != 0
//
//        return isStatusBarHidden and isNavigationBarHidden
//    }


    /**
     * 设置沉浸式模式
     */
    private fun setImmersiveUIMode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.setDecorFitsSystemWindows(false)
        } else {
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        }

        window.statusBarColor = Color.TRANSPARENT
        window.navigationBarColor = Color.TRANSPARENT
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION

    }


    protected abstract fun getLayoutId(): Int
    protected abstract fun init()
}